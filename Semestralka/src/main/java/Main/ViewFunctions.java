/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class ViewFunctions {
      public static void deleteAllTableRows(javax.swing.JTable Table) {
        DefaultTableModel model = (DefaultTableModel) Table.getModel();
        int rows = Table.getRowCount() - 1;
        if (rows >= 0) {
            for (int i = rows; i >= 0; i--) {
                model.removeRow(i);
            }
        }
    }   
}
