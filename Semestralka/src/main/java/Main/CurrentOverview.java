/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Dto.CurrentSituationDTO;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import Model.CurrentSituationModel;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class CurrentOverview  extends JFrame {
    
    /** 
     * Variables declaration
     */
    private final JPanel panel = new JPanel();
    private final JLabel labelHour = new JLabel();
    private final JSpinner hour = new JSpinner();
    private final JLabel labelMin = new JLabel();
    private final JSpinner min = new JSpinner();
    private final JLabel labelDay = new JLabel();
    private final JComboBox comboBoxDay = new JComboBox<>();
    private final JButton search = new JButton();
    private final JScrollPane scrollPane = new JScrollPane();
    private final JTable table = new JTable();
    DefaultTableModel model;

    public CurrentOverview(String title) {
        super(title);
        initComponents();
        model = (DefaultTableModel) table.getModel();
        this.setVisible(true);
    }
    
    /** 
     * Initialization components, creation view
     */
    private void initComponents() {
                
        /** 
         * setup JFrame, dispose on close, dimension 500x400, not resizable
         */
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setMaximumSize(new Dimension(500, 400));
        setMinimumSize(new Dimension(500, 400));
        setPreferredSize(new Dimension(500, 400));
        setResizable(false);
        
        /**
         * setup JPanel, white background, set GroupLayout
         */
        panel.setBackground(new Color(255, 255, 255));
        GroupLayout panelLayout = new GroupLayout(panel);
        panel.setLayout(panelLayout);
        
        /**
         * setup JPanel horizontal arrangement, add components to JPanel and set gaps
         */
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(search, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(labelHour)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hour, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(labelMin)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(min, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(labelDay)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBoxDay, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        /**
         * setup JPanel vertical arrangement, add components to JPanel
         */
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(panelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(labelHour)
                    .addComponent(labelMin)
                    .addComponent(labelDay)
                    .addComponent(hour, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(min, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxDay, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(search, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 253, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        
        /**
         * setup components, theirs font, text, dimension
         */
        labelHour.setFont(new Font("Tahoma", 0, 14));
        labelHour.setText("Hodiny:");
        
        hour.setModel(new javax.swing.SpinnerNumberModel(0, 0, 23, 1));
        hour.setPreferredSize(new Dimension(50, 25));
        
        labelMin.setFont(new Font("Tahoma", 0, 14));
        labelMin.setText("Minuty:");
        
        min.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));
        min.setPreferredSize(new Dimension(50, 25));
        
        labelDay.setFont(new Font("Tahoma", 0, 14));
        labelDay.setText("Den:");
        
        comboBoxDay.setModel(new DefaultComboBoxModel<>(new String[] { "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle" }));
        comboBoxDay.setPreferredSize(new Dimension(80, 25));
        
        search.setFont(new Font("Tahoma", 0, 14));
        search.setText("HLEDAT");
        search.setPreferredSize(new java.awt.Dimension(90, 30));
        /**
         * add ActionListener to search button
         */
        search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchActionPerformed(evt);
            }
        });
        
        /**
         * setup scrollpane/table, header
         */
        table.setFont(new Font("Tahoma", 0, 14));
        table.setModel(new DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Linka číslo", "Příští zastávka (+ min)", "Autobus"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        scrollPane.setViewportView(table);
        
        /**
         * setup JFrame layout to GroupLayout and add JPanel component
         */
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        /**
         * set location center to screen
         */
        setLocationRelativeTo(null);
        
    }
    
    /**
     * initialized day from ComboBox, hours and minutes from JSpinner
     * @param evt 
     */
    private void searchActionPerformed(java.awt.event.ActionEvent evt) {                                         

        int day = comboBoxDay.getSelectedIndex()+1;
        int hours = hour.getValue().hashCode();
        int minutes = min.getValue().hashCode();
        setTableRows(day, hours, minutes);
        
    }
    
    /**
     * set ScrollPane/Table rows, first clear table, then add rows
     * computation minutes to go from current busStop to the next station
     * 
     * @param day
     * @param hours
     * @param minutes 
     */
    private void setTableRows(int day, int hours, int minutes) {
        ViewFunctions.deleteAllTableRows(table);
        CurrentSituationModel currentSit = new CurrentSituationModel();
        ArrayList<CurrentSituationDTO> currentSituationList =  currentSit.getList(day, hours, minutes);
        for (CurrentSituationDTO item : currentSituationList) {
            int currentMinutes = hours*60 + minutes;
            int nextMinutes = item.getTimeArrived().getHours()*60 + item.getTimeArrived().getMinutes();
            int minutesToGo = nextMinutes - currentMinutes;
            model.addRow(new Object[]{item.getLine(), item.getNextBusStop()+" (+ "+minutesToGo+")", item.getBus()});
        }
    }
    
}
