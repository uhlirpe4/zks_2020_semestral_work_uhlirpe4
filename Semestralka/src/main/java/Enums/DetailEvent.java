/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public enum DetailEvent {
    UPDATE,
    ADD,
    GETLIST;
}
