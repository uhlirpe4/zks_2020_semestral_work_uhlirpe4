/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public enum BusStopCombo {     
    Signification ("Název","Signification"),
    Street ("Ulice","Street"),
    Town ("Město","Town");  
    private final String value;    
    private final String comboName;
    BusStopCombo (String comboName, String value) {
        this.comboName = comboName;
        this.value = value;
    }
    public String value() {
        return value;
    }
    public String comboName() {
        return comboName;
    }
}
