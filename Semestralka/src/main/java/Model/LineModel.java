/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.DeleteReaction;
import Dto.LineDTO;
import Dto.StopOnRouteDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class LineModel extends BaseClass<LineDTO> {
    private String oldLineName;
    /**
     *
     * @param choice
     * @param text
     * @return
     */
    public ArrayList<LineDTO> getLineList(int choice, String text) {
        ArrayList array = new ArrayList();
        array.add(choice);
        array.add(text);
        return List((Object) array);
    }
    
    /**
     *
     * @param idName
     * @param array
     */
    public void InsertLine(String idName, ArrayList<StopOnRouteDTO> array) {
        try {
            LineDTO line = new LineDTO();
            line.setNumber(idName);
            conn.setAutoCommit(false);
            if (Insert(line)) {
                if (InsertRoutes(array)) {
                    conn.commit();
                } else {
                    conn.rollback();
                }
            } else {
                conn.rollback();
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (conn != null) {
                    conn.setAutoCommit(true);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    /**
     * get stopOnRoute on line and busStop
     * @param line
     * @param busStop
     * @return
     */
    public StopOnRouteDTO getStopOnRoute(String line, String busStop) {
        ArrayList<StopOnRouteDTO> stopsOnRoute = getStopsOnRouteList(line);        
        for (int i = 0; i < stopsOnRoute.size(); i++) {
            if (stopsOnRoute.get(i).getBusStop().equals(busStop)) {
                return stopsOnRoute.get(i);
            }
        }
        return null;
    }

    /**
     * get previous stopOnRoute on line
     * @param line
     * @param busStop
     * @return
     */
        public StopOnRouteDTO getPrevStopOnRoute(String line, String busStop) {
        ArrayList<StopOnRouteDTO> stopsOnRoute = getStopsOnRouteList(line);        
        for (int i = 0; i < stopsOnRoute.size(); i++) {
            if (stopsOnRoute.get(i).getBusStop().equals(busStop)) {
                return stopsOnRoute.get(i-1);
            }
        }
        return null;
    }    
    /**
     *  get next stopOnRoute on line
     * @param line
     * @param busStop
     * @return
     */
        public StopOnRouteDTO getNextStopOnRoute(String line, String busStop) {
       ArrayList<StopOnRouteDTO> stops = getStopsOnRouteList(line);   
        for (int i = 0; i < stops.size() - 1; i++) {
            if (stops.get(i).getBusStop().equals(busStop)) {
                return stops.get(i + 1);                
            }
        }      
        return null;       
    }   
    /**
     * Insert Stop on the route which belongs to lines
     *
     * @param array
     * @return
     * @throws SQLException
     */
    public boolean InsertRoutes(ArrayList<StopOnRouteDTO> array) throws SQLException {
        for (StopOnRouteDTO stopOnRoute : array) {
            String sql
                    = "INSERT INTO StopOnRoute"
                    + " (Line,busStop, BusStopNumber, timeFromPrevBusStop)"
                    + "VALUES (?,?,?,?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, stopOnRoute.getLine());
            prepare.setString(2, stopOnRoute.getBusStop());
            prepare.setInt(3, stopOnRoute.getBusStopNumber());
            prepare.setInt(4, stopOnRoute.getTimeFromPrevBusStop());
            prepare.execute();
        }
        return true;
    }

    /**
     *
     * @param array
     * @return
     * @throws SQLException
     */
    public boolean UpdateRoutes(ArrayList<StopOnRouteDTO> array) throws SQLException {
       for (StopOnRouteDTO stopOnRoute : array) {
            String sql
                    = "UPDATE StopOnRoute "
                    + "SET Line = ? ,busStop = ? ,BusStopNumber = ? ,timeFromPrevBusStop= ? "
                    + "WHERE Line = ? AND BusStop = ? ";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, stopOnRoute.getLine());
            prepare.setString(2, stopOnRoute.getBusStop());
            prepare.setInt(3, stopOnRoute.getBusStopNumber());
            prepare.setInt(4, stopOnRoute.getTimeFromPrevBusStop());            
             prepare.setString(5, stopOnRoute.getLine());
             prepare.setString(6, stopOnRoute.getBusStop());
            prepare.execute();
        }
        return true;  
    }
    /**
     *
     * @param line
     * @param BusStop
     * @return
     */
     public DeleteReaction DeleteStopOnRoute(String line,String BusStop) {
        try {
            String sql
                    = "DELETE FROM StopOnRoute"
                    + " WHERE line = ? AND BusStop = ?"
                    + ";";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, line);
            pstmt.setString(2, BusStop);
            pstmt.execute();
            BaseModelClass.CloseStmt(null, pstmt);
            return DeleteReaction.TRUE;
        } catch (SQLException e) {
            System.out.print(e.getMessage());            
            return DeleteReaction.FALSE;
        }       
    }
     
    /**
     *
     * @param lineIndication
     * @return
     */
    public DeleteReaction DeleteLine(String lineIndication) {
        LineDTO line = new LineDTO();
        line.setNumber(lineIndication);
        return Delete(line);
    }

    /**
     * get list of BusStops on the route of line
     * @param line
     * @return
     */
    public ArrayList<StopOnRouteDTO> getStopsOnRouteList(String line) {
        try {
            ArrayList<StopOnRouteDTO> array = new ArrayList<>();
            PreparedStatement pstmt = BeforeListStopsOnRoute(line);
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                array.add(getDataStopsOnRoute(res));
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return array;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param IdName
     * @return
     */
    public boolean ValidateInsertLine(String IdName) {
        LineDTO line = new LineDTO();
        line.setNumber(IdName);
        return ValidateUpdate(line);
    }

    /**
     *
     * @param idName
     * @param stopsOnRoute
     * @param oldLineName
     */
    public void UpdateLine(String idName, ArrayList<StopOnRouteDTO> stopsOnRoute, String oldLineName ){
        try{
            LineDTO line = new LineDTO();
            this.oldLineName = oldLineName;
            line.setNumber(idName);
            conn.setAutoCommit(false);
            if (Update(line)) {
                if (UpdateRoutes(stopsOnRoute)) {
                    conn.commit();
                } else {
                    conn.rollback();
                }
            } else {
                conn.rollback();
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (conn != null) {
                    conn.setAutoCommit(true);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }   
    }
    @Override
    public PreparedStatement BeforeUpdate(LineDTO data) {
            try {                
            String sql = "UPDATE Line"
                    + " SET IdName = ? "
                    + " WHERE IdName = ? ; ";

            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getNumber());
            prepare.setString(2, oldLineName);
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    
    @Override
    public PreparedStatement BeforeValidateUpdate(LineDTO data) {
        try {
            String sql = "SELECT * FROM Line WHERE IdName =? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getNumber());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeList(Object o) {
        try {
            ArrayList array = (ArrayList) o;
            int choice = (int) array.get(0);
            String text = (String) array.get(1);
            String filter;
            switch (choice) {              
                default:
                    filter = "IdName";
                    break;
            }
            String sql = "SELECT idName, "
                    + "(select busstop from StopOnRoute where line = sLine.idName order by BusStopNumber limit 1) as FirstStop, "
                    + "(select busstop from StopOnRoute where line = sLine.idName order by BusStopNumber desc limit 1) as LastStop "
                    + "FROM Line sLine "
                    + " WHERE IdName LIKE '%" + text + "%'"
                    + " ORDER BY IdName ASC ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public LineDTO getData(ResultSet res) {
        try {
            LineDTO line = new LineDTO();
            line.setNumber(res.getString("IdName"));
            line.setFirstBusStop(res.getString("FirstStop"));
            line.setLastBusStop(res.getString("LastStop"));
            return line;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement beforeInsert(LineDTO data) {
        try {
            String sql
                    = "INSERT INTO Line"
                    + " (IdName)"
                    + "VALUES (?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getNumber());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeDelete(LineDTO data) {
        try {
            String sql
                    = "DELETE FROM Line"
                    + " WHERE IdName = ?"
                    + ";";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getNumber());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    // part of list BusStopsOnRoute with sql select 
    private PreparedStatement BeforeListStopsOnRoute(String line) {
        try {
            String sql = "SELECT * FROM StopOnRoute WHERE line= ? ORDER BY BusStopNumber ASC ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, line);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    // get data from StopsOnRoute
    private StopOnRouteDTO getDataStopsOnRoute(ResultSet res) {
        try {
            StopOnRouteDTO stops = new StopOnRouteDTO();
            stops.setBusStop(res.getString("busStop"));
            stops.setBusStopNumber(res.getInt("busStopNumber"));
            stops.setLine(res.getString("Line"));
            stops.setTimeFromPrevBusStop(res.getInt("TimeFromPrevBusStop"));
            return stops;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

}
