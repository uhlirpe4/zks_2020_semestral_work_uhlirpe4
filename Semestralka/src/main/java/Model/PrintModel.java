/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.Order;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import Dto.BusConnectionDTO;
import Dto.LineDTO;
import Dto.StopOnRouteDTO;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class PrintModel {

    /**
     *
     */
    protected Connection conn;
    BusConnectionModel busConnMod = new  BusConnectionModel();
    String choosenLine;
    String choosenBusStop;
    String[] timetable = new String[168];
    
    /**
     * Connection to database
     */  
    public PrintModel() {
        DBConnection dbConnection = new DBConnection();
        conn = dbConnection.connect();
    }
    
    /**
     * Main function to create pdf.
     * 
     * @param dest - file save destination which you choose
     * @param line
     * @param busStop
     * @throws FileNotFoundException
     * @throws DocumentException 
     */
    public void createPdf(String dest, String line, String busStop) throws FileNotFoundException, DocumentException {
        choosenLine = line;
        choosenBusStop = busStop;
        int routeTime = 0;
        boolean routeTimeStart = false;
        
        /**
         * Document properties
         */
        Document document = new Document(PageSize.A4.rotate(), 5, 5, 15, 15);
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        Font titleFont = FontFactory.getFont(FontFactory.TIMES_BOLD, "Cp1250", true, 24, Font.BOLD);
        Font chapterFont = FontFactory.getFont(FontFactory.TIMES_BOLD, "Cp1250", true, 22);
        Font paragraphFont = FontFactory.getFont(FontFactory.TIMES, "Cp1250", true, 12, Font.NORMAL);
        Chunk headTitle = new Chunk("Jízdní řád", titleFont);
        Chunk title = new Chunk(String.format("| linka: %s | zastávka: %s |", line, busStop), chapterFont);
        
        PdfPTable table = new PdfPTable(10);
        table.setWidthPercentage(100);
        table.setWidths(new int[]{5, 1, 1, 2, 2, 2, 2, 2, 2, 2});
        table.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        
        addTableHeader(table);
        addTableUnits(table);
        
        LineModel lineModel= new LineModel();
        ArrayList<StopOnRouteDTO> stopsOnRoute = lineModel.getStopsOnRouteList(line);
        
        ArrayList<BusConnectionDTO> busConnectionsWithAddTime =  getReportLineBusConnections(choosenLine, choosenBusStop, Order.TIMEDAY);
        
        int index = 0;
        /**
         * Inserting minutes or nothing to timetable array ... it is the easiest way to fill all cells in the pdf table
         * If are more than one busConnection in the same hour then they are add to the same cell
         */
        for (int k = 0; k < 168; k++) {
            if (index < busConnectionsWithAddTime.size()) {
                if (busConnectionsWithAddTime.get(index).getDay() == (k%7)+1 && busConnectionsWithAddTime.get(index).getTime().getHours() == k/7){
                    timetable[k] = String.valueOf(busConnectionsWithAddTime.get(index).getTime().getMinutes());
                    index++;
                    while (index < busConnectionsWithAddTime.size()) {
                        if (busConnectionsWithAddTime.get(index).getDay() == (k%7)+1 && busConnectionsWithAddTime.get(index).getTime().getHours() == k/7){
                            timetable[k] += ", "+String.valueOf(busConnectionsWithAddTime.get(index).getTime().getMinutes());
                            index++;
                        } else {
                            break;
                        }
                    }
                } else {
                    timetable[k] = "";
                }
            } else {
                timetable[k] = "";
            }
        }
        
        index = 0;
        /**
         * Inserting rows to the table...
         * 
         * Inserting busStops with timeFromPreviousBusStop, routeTime which is counting from chooseBusStop, hours from 0 to 23 and minutes from disposed array timetable.
         * If stopsOnRoute are more than 24(hour), then the time isn't insert and continues with busStops inserting
         */
        if (stopsOnRoute.size() <= 24){
            for (int i = 0; i < 24; i++) {
                if (i < stopsOnRoute.size()) {
                    if (stopsOnRoute.get(i).getBusStop().equals(choosenBusStop)) {
                        routeTimeStart = true;
                        addRow(table, stopsOnRoute.get(i).getBusStop()+" (+ "+stopsOnRoute.get(i).getTimeFromPrevBusStop()+")", String.valueOf(routeTime), String.valueOf(i), timetable[index], timetable[index+1], timetable[index+2], timetable[index+3], timetable[index+4], timetable[index+5], timetable[index+6], true);
                        index += 7;
                    } else {
                        if (routeTimeStart) {
                            routeTime += stopsOnRoute.get(i).getTimeFromPrevBusStop();
                        } else {
                        }
                        addRow(table, stopsOnRoute.get(i).getBusStop()+" (+ "+stopsOnRoute.get(i).getTimeFromPrevBusStop()+")", String.valueOf(routeTime), String.valueOf(i), timetable[index], timetable[index+1], timetable[index+2], timetable[index+3], timetable[index+4], timetable[index+5], timetable[index+6], false);
                        index += 7;
                    }
                } else {
                    addRow(table, "", "", String.valueOf(i), timetable[index], timetable[index+1], timetable[index+2], timetable[index+3], timetable[index+4], timetable[index+5], timetable[index+6], false);
                    index += 7;
                }
            }
        } else {
            for (int j = 0; j < stopsOnRoute.size(); j++) {
                if (j < 24) {
                    if (stopsOnRoute.get(j).getBusStop().equals(choosenBusStop)) {
                        routeTimeStart = true;
                        addRow(table, stopsOnRoute.get(j).toString(), String.valueOf(routeTime), String.valueOf(j), timetable[index], timetable[index+1], timetable[index+2], timetable[index+3], timetable[index+4], timetable[index+5], timetable[index+6], true);
                        index += 7;
                    } else {
                        if (routeTimeStart) {
                            routeTime += stopsOnRoute.get(j).getTimeFromPrevBusStop();
                        } else {
                        }
                        addRow(table, stopsOnRoute.get(j).toString(), String.valueOf(routeTime), String.valueOf(j), timetable[index], timetable[index+1], timetable[index+2], timetable[index+3], timetable[index+4], timetable[index+5], timetable[index+6], false);
                        index += 7;
                    }
                } else {
                    if (stopsOnRoute.get(j).getBusStop().equals(choosenBusStop)) {
                        routeTimeStart = true;
                        addRow(table, stopsOnRoute.get(j).toString(), String.valueOf(routeTime), "", "", "", "", "", "", "", "", true);
                    } else {
                        if (routeTimeStart) {
                            routeTime += stopsOnRoute.get(j).getTimeFromPrevBusStop();
                        } else {
                        }
                        addRow(table, stopsOnRoute.get(j).toString(), String.valueOf(routeTime), "", "", "", "", "", "", "", "", false);
                    }
                }
            }
        }
        
        /**
         * Inserting document head title, title with line & choosenBusStop and the table
         */        
        document.add(headTitle);
        document.add(new Phrase("\n"));
        document.add(new Phrase("\n"));
        document.add(title);
        document.add(new Phrase("\n"));
        document.add(table);
        document.close();

    }
    
    /**
     * Add table header - the first row in the pdf table
     * 
     * @param table 
     */
    private void addTableHeader(PdfPTable table) {
        Font fontBold = FontFactory.getFont(FontFactory.TIMES, "Cp1250", true, 12, Font.BOLD);
        Stream.of("SEZNAM ZASTÁVEK", "jízda", "", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle")
          .forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setMinimumHeight(50);
            header.setHorizontalAlignment(Element.ALIGN_CENTER);
            header.setVerticalAlignment(Element.ALIGN_MIDDLE);
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            Phrase phraseHeader = new Phrase();
            phraseHeader.add(new Chunk(columnTitle,  fontBold));
            header.setPhrase(phraseHeader);
            table.addCell(header);
            });
    }
    
    /**
     * Add second row with table units
     * 
     * @param table 
     */
    private void addTableUnits(PdfPTable table) {
        Font fontBold = FontFactory.getFont(FontFactory.TIMES, "Cp1250", true, 12, Font.BOLD);
        Stream.of("následujicí zastávky (+ min)", "min", "hod", "min", "min", "min", "min", "min", "min", "min")
          .forEach(columnTitle -> {
            PdfPCell units = new PdfPCell();
            units.setMinimumHeight(20);
            units.setBackgroundColor(BaseColor.LIGHT_GRAY);
            units.setHorizontalAlignment(Element.ALIGN_CENTER);
            units.setVerticalAlignment(Element.ALIGN_MIDDLE);
            Phrase phraseUnits = new Phrase();
            phraseUnits.add(new Chunk(columnTitle,  fontBold));
            units.setPhrase(phraseUnits);
            table.addCell(units);
            });
    }
    
    /**
     * Add one row (separete cells) to pdf table, if busStop is choosenBusStop, then the busStop is bold
     * 
     * @param table
     * @param busStop
     * @param route
     * @param hour
     * @param pondeli
     * @param utery
     * @param streda
     * @param ctvrtek
     * @param patek
     * @param sobota
     * @param nedele
     * @param choosen 
     */
    private void addRow(PdfPTable table, String busStop, String route, String hour, String pondeli, String utery, String streda, String ctvrtek, String patek, String sobota, String nedele, Boolean choosen) {
        Font font = FontFactory.getFont(FontFactory.TIMES, "Cp1250", true, 12, Font.NORMAL);
        Font boldFont = FontFactory.getFont(FontFactory.TIMES, "Cp1250", true, 12, Font.BOLD);
        
        Phrase phraseBusStopBold = new Phrase();
        phraseBusStopBold.add(new Chunk(busStop,  boldFont));
        Phrase phraseBusStop = new Phrase();
        phraseBusStop.add(new Chunk(busStop,  font));
        Phrase phraseRoute = new Phrase();
        phraseRoute.add(new Chunk(route,  font));
        Phrase phraseHour = new Phrase();
        phraseHour.add(new Chunk(hour,  font));
        Phrase phrasePondeli = new Phrase();
        phrasePondeli.add(new Chunk(pondeli,  font));
        Phrase phraseUtery = new Phrase();
        phraseUtery.add(new Chunk(utery,  font));
        Phrase phraseStreda = new Phrase();
        phraseStreda.add(new Chunk(streda,  font));
        Phrase phraseCtvrtek = new Phrase();
        phraseCtvrtek.add(new Chunk(ctvrtek,  font));
        Phrase phrasePatek = new Phrase();
        phrasePatek.add(new Chunk(patek,  font));
        Phrase phraseSobota = new Phrase();
        phraseSobota.add(new Chunk(sobota,  font));
        Phrase phraseNedele = new Phrase();
        phraseNedele.add(new Chunk(nedele,  font));
        
        if (choosen == true) {
            table.addCell(phraseBusStopBold);
        } else {
            table.addCell(phraseBusStop);
        }
        table.addCell(phraseRoute);
        table.addCell(phraseHour);
        table.addCell(phrasePondeli);
        table.addCell(phraseUtery);
        table.addCell(phraseStreda);
        table.addCell(phraseCtvrtek);
        table.addCell(phrasePatek);
        table.addCell(phraseSobota);
        table.addCell(phraseNedele);
    }
    
    /**
     * All bus stop on the route with times from previous
     * @param busStop
     * @return
     */
    public ArrayList<LineDTO> getListLinesOnBusStop(String busStop){
      try {  
            ArrayList<LineDTO> array = new ArrayList<LineDTO>();          
            String sql = "SELECT line FROM StopOnRoute WHERE BusStop = ? ";            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, busStop);
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
               LineDTO line = new LineDTO();
               line.setNumber(res.getString("line"));
               array.add(line);
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return array;            
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }    
    }

    /**
     * get information to time table report  
     * @param ChoosenLine
     * @param ChoosenBusStop
     * @return
     */
    public ArrayList<BusConnectionDTO> getReportLineBusConnections(String ChoosenLine, String ChoosenBusStop, Order order){
        LineModel lineModel= new LineModel();  
        ArrayList<StopOnRouteDTO> stopsOnRoute= lineModel.getStopsOnRouteList(ChoosenLine);
        ArrayList<BusConnectionDTO> allBusConnections;
         if(order== Order.DAYTIME)
        {
        allBusConnections = getAllLineBusConnectionsDayTimeOrder(ChoosenLine);
        }
         else{
        allBusConnections =  getAllLineBusConnections(ChoosenLine); 
        }             
        Time addTime = sumTimeFromStartToBusStop(stopsOnRoute,ChoosenBusStop);       
        ArrayList<BusConnectionDTO> BusConnWithAddTime = ActualizeTimesToBusStop(allBusConnections,addTime);        
        return BusConnWithAddTime;
    }

    /**
     * get busConnections with right time of arrive to the chosen BusStop
     * @param busConnections
     * @param addTime
     * @return
     */
    public ArrayList<BusConnectionDTO> ActualizeTimesToBusStop( ArrayList<BusConnectionDTO> busConnections, Time addTime){
       for(BusConnectionDTO busCon : busConnections){         
//        busCon.setTime(busConnMod.timeSum( busCon.getTime(), addTime));
//        int plusDay = busConnMod.getPlusDay();
//        if(plusDay!=0){
//        int day = (busCon.getDay()+plusDay)%7 ;
//        if(day==0){
//        day = 7;
//        }          
//        busCon.setDay(day);       
//        }   
         ActualizeTimeToBusStop(busCon,addTime);
        }
       return busConnections;
    }  

    /**
     * function add time to bussConection time 
     * @param busConnections
     * @param addTime
     * @return
     */
    public BusConnectionDTO ActualizeTimeToBusStop( BusConnectionDTO busConnections, Time addTime){
        busConnections.setTime(busConnMod.timeSum( busConnections.getTime(), addTime));
        int plusDay = busConnMod.getPlusDay();
        if(plusDay!=0){
        int day = (busConnections.getDay()+plusDay)%7 ;
        if(day==0){
        day = 7;
        }          
        busConnections.setDay(day);       
        }         
       return busConnections;
    }
     
    /**
     * function get all BusConnections in the order (time, Day ASC)
     * @param line
     * @return
     */
    public  ArrayList<BusConnectionDTO> getAllLineBusConnections(String line){
               try {  
            ArrayList<BusConnectionDTO> array = new ArrayList<BusConnectionDTO>();          
            String sql = "SELECT * FROM lineStartTime WHERE line = ? ORDER BY time, Day";            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, line);
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                
               BusConnectionDTO busConn = busConnMod.getData(res);                             
               array.add(busConn);
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return array;            
          } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }       
    }
      public  ArrayList<BusConnectionDTO> getAllLineBusConnectionsDayTimeOrder(String line){
               try {  
            ArrayList<BusConnectionDTO> array = new ArrayList<BusConnectionDTO>();          
            String sql = "SELECT * FROM lineStartTime WHERE line = ? ORDER BY Day, Time";            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, line);
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                
               BusConnectionDTO busConn = busConnMod.getData(res);                             
               array.add(busConn);
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return array;            
          } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }       
    }
    /**
     * return time, which bus needs to achieve from start of route to choosen busStop
     * @param stopsOnRoute
     * @param ChoosenBusStop
     * @return
     */
    public Time sumTimeFromStartToBusStop( ArrayList<StopOnRouteDTO> stopsOnRoute,String ChoosenBusStop){
        int timeFromStart = 0; 
        for(StopOnRouteDTO stopOnRoute : stopsOnRoute)
        {
            timeFromStart+=stopOnRoute.getTimeFromPrevBusStop();
            if(stopOnRoute.getBusStop().equals(ChoosenBusStop)){
             break;
            }             
        }
        return new Time(0,timeFromStart,0);
    }

    /**
     * function get Time From previousBusStop
     * @param stopsOnRoute
     * @param ChoosenBusStop
     * @return
     */
    public Time sumTimeFromPrevBusStop( ArrayList<StopOnRouteDTO> stopsOnRoute,String ChoosenBusStop){
        int timeFromprev = 0; 
        for(StopOnRouteDTO stopOnRoute : stopsOnRoute)
        {           
            if(stopOnRoute.getBusStop().equals(ChoosenBusStop)){
             timeFromprev=stopOnRoute.getTimeFromPrevBusStop();
             break;
            }             
        }
        return new Time(0,timeFromprev,0);
    }
}

