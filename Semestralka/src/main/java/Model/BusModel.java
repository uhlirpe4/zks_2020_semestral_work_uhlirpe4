/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.DeleteReaction;
import Enums.SpecialTexts;
import com.sun.javafx.scene.text.TextSpan;
import Dto.BusDTO;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusModel extends BaseClass<BusDTO> {

    String oldIndication;

    /**
     *
     * @param indication
     * @param busType
     * @param CarPark
     */
    public void InsertBus(String indication, String busType, String CarPark) {
        BusDTO busStop = new BusDTO();
        busStop.setIndication(indication);
        busStop.setBusType(busType);
        busStop.setCarPark(CarPark);          
        Insert(busStop);
    }

    /**
     *
     * @param indication
     * @return
     */
    public boolean ValidateUpdateBus(String indication) {
        BusDTO busStop = new BusDTO();
        busStop.setIndication(indication);
        return ValidateUpdate(busStop);
    }

    /**
     *
     * @param indication
     * @param carPark
     * @param busType
     * @param oldIndication
     */
    public void UpdateBus(String indication, String carPark, String busType, String oldIndication) {
        BusDTO bus = new BusDTO();
        bus.setIndication(indication);
        bus.setCarPark(carPark);
        bus.setBusType(busType);
        this.oldIndication = oldIndication;
        Update(bus);
    }

    /**
     *
     * @param busType
     * @param indic
     * @return
     */
    public ArrayList<BusDTO> getBusList(String busType, String indic) {
        ArrayList array = new ArrayList();
        array.add(busType);
        array.add(indic);
        return List((Object) array);
    }

    /**
     *
     * @param indication
     * @return 
     */
    public DeleteReaction DeleteBus(String indication) {
        BusDTO bus = new BusDTO();
        bus.setIndication(indication);
        return Delete(bus);
    }

    @Override
    public PreparedStatement BeforeUpdate(BusDTO data) {
        try {
            String sql = "UPDATE Bus"
                    + " SET Indication = ? , busType = ? , CarPark = ?"
                    + " WHERE Indication =?";

            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getIndication());
            prepare.setString(2, data.getBusType());
            prepare.setString(3, data.getCarPark());
            prepare.setString(4, oldIndication);
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeList(Object o) {
        try {
            ArrayList array = (ArrayList) o;
            String busType = (String) array.get(0);
            if (busType.equals(SpecialTexts.ALLTYPES.value())) {
                busType = "";
            }
            String Indication = (String) array.get(1);
            String sql = "SELECT * FROM Bus WHERE BusType LIKE '%" + busType + "%' AND Indication LIKE '%" + Indication + "%';";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public BusDTO getData(ResultSet res) {
        try {
            BusDTO bus = new BusDTO();
            bus.setBusType(res.getString("busType"));
            bus.setCarPark(res.getString("carPark"));
            bus.setIndication(res.getString("indication"));
            return bus;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeValidateUpdate(BusDTO data) {
        try {
            String sql = "SELECT * FROM Bus WHERE indication=? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getIndication());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeDelete(BusDTO bus) {
        try {
            ResultSet res = null;
            PreparedStatement pstmt = null;
            String sql
                    = "DELETE FROM Bus"
                    + " WHERE Indication = ? ;";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, bus.getIndication());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement beforeInsert(BusDTO data) {
        try {
            String sql
                    = "INSERT INTO Bus"
                    + " (indication, busType, CarPark)"
                    + "VALUES (?,?,?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getIndication());
            prepare.setString(2, data.getBusType());
            prepare.setString(3, data.getCarPark());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeGetById(BusDTO data) {
        try {
            String sql = "SELECT * FROM Bus WHERE indication=?";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getIndication());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
}
