/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Dto.BusConnectionDTO;
import Dto.CurrentSituationDTO;
import Dto.StopOnRouteDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class CurrentSituationModel {

    private final Connection conn;

    public CurrentSituationModel() {
        DBConnection dbConn = new DBConnection();
        conn = dbConn.connect();
    }

    /**
     * return list of current situations(current situation of bussConnections) 
     * @param day
     * @param hours
     * @param minutes
     * @return
     */
    public ArrayList<CurrentSituationDTO> getList(int day, int hours, int minutes) {
        try {
            ArrayList<CurrentSituationDTO> result = new ArrayList<>();
            Time selectedTime = new Time(hours, minutes, 0);
            BusConnectionModel busConnMod = new BusConnectionModel();        
            ArrayList<BusConnectionDTO> arrayLineAndTime = busConnMod.getTimeAndLineArray(null);
            //foreach line find busConnection on the route in the selected time
            for (BusConnectionDTO item : arrayLineAndTime) {
                Time startTime = busConnMod.timeSubstr(selectedTime, item.getTimeOfRoute());
                int startDay = day - busConnMod.getMinusStartDay();
                if (startDay < 0) {
                    startDay = startDay % 7;
                    if (startDay == 0) {
                        startDay = 7;
                    }
                }
                String sql = "";
                // if day of foreign connection start is bigger than day of connection, you must find out days separately to the end of week, and then from the start 
                if (startDay > day) {
                    sql = "SELECT * FROM lineStartTime WHERE (( Day BETWEEN ? AND 7 )OR ( Day BETWEEN 0 AND ?))  AND ( Time BETWEEN ? AND ? ) AND Line = ?;";
                } else {
                    sql = "SELECT * FROM lineStartTime WHERE ( Day BETWEEN ? AND ? ) AND ( Time BETWEEN ? AND ? ) AND Line = ?;";
                }
                PreparedStatement pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, startDay);
                pstmt.setInt(2, day);
                pstmt.setTime(3, startTime);
                pstmt.setTime(4, selectedTime);
                pstmt.setString(5, item.getLine());
                ResultSet res = pstmt.executeQuery();
                while (res.next()) {
                    //set current situation and add to list
                    CurrentSituationDTO curSit = new CurrentSituationDTO();
                    curSit.setBus(res.getString("Bus"));
                    curSit.setLine(res.getString("Line"));                    
                    Time timeStartLine = res.getTime("time");  
                    CurrentSituationDTO curSitRes = getBusStopTimeArrive(curSit,timeStartLine,selectedTime);
                    if(curSitRes!=null) {
                    result.add(getBusStopTimeArrive(curSit,timeStartLine,selectedTime));
                    }
                }
            }
            return result;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    } 
    
    /**
     * get time and BusStop signification when bus arrived to nextStation on the route
     * @param curSit
     * @param timeStartLine
     * @param selectedTime
     * @return
     */
    public CurrentSituationDTO  getBusStopTimeArrive(CurrentSituationDTO curSit,Time timeStartLine, Time selectedTime){
                  BusConnectionModel busConnMod= new BusConnectionModel();
                  LineModel lineMod = new LineModel();
                  ArrayList<StopOnRouteDTO> routeBusStops = lineMod.getStopsOnRouteList(curSit.getLine());
                    int index = 0;
                    // find busstop and time, when is nearest to selected time
                    while (timeStartLine.before(selectedTime)) {                       
                        StopOnRouteDTO stopOnRoute = routeBusStops.get(index);
                        Time addTime = new Time(0, stopOnRoute.getTimeFromPrevBusStop(), 0);
                        timeStartLine = busConnMod.timeSum(timeStartLine,addTime);  
                         index++;
                    }
                    if(routeBusStops.size()<index){
                     return null;
                    }
                    curSit.setNextBusStop(routeBusStops.get(index-1).getBusStop());                    
                    curSit.setTimeArrived(timeStartLine);
                    return curSit;
    }
  
}
