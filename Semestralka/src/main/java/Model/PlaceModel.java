/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.DeleteReaction;
import Dto.BusStopDTO;
import Dto.PlaceDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class PlaceModel extends BaseClass<PlaceDTO> {

    /**
     *
     */
    public PlaceModel() {
    }

    /**
     * get id from different key(street,town)
     * @param street
     * @param town
     * @return
     */
    public int getPlaceId(String street, String town) {
        String sql
                = "SELECT IdPlace"
                + " FROM Place"
                + " WHERE Street='" + street + "' AND Town='" + town + "';";
        String getColumn = "IdPlace";
        return BaseClass.getIdInt(sql, conn, getColumn);
    }

    /**
     * 
     * @param idPlace
     * @return
     */
    public PlaceDTO getPlaceById(int idPlace) {
        try {
            PlaceDTO place = new PlaceDTO();
            String sql
                    = "SELECT Street,Town"
                    + " FROM Place"
                    + " WHERE idPlace='" + idPlace + "';";

            PreparedStatement pstmt = null;
            ResultSet res = null;
            pstmt = conn.prepareStatement(sql);
            res = pstmt.executeQuery();

            while (res.next()) {
                place.setStreet(res.getString("Street"));
                place.setTown(res.getString("Town"));
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return place;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;
    }

    /**
     *
     * @param street
     * @param town
     * @return
     */
    public DeleteReaction DeletePlace(String street, String town) {
        PlaceDTO place = new PlaceDTO();
        place.setStreet(street);
        place.setTown(town);
        return Delete(place);
    }

    /**
     *
     * @param street
     * @param town
     */
    public void InsertPlace(String street, String town) {
        PlaceDTO place = new PlaceDTO();
        place.setStreet(street);
        place.setTown(town);
        Insert(place);
    }
    
    @Override
    public PreparedStatement BeforeDelete(PlaceDTO data) {
        try {
            int id = getPlaceId(data.getStreet(), data.getTown());
            String sql
                    = "DELETE FROM Place"
                    + " WHERE IdPlace"
                    + " NOT IN ( SELECT BusStop.IdPlace FROM CarPark Inner JOIN BusStop ON BusStop.IdPlace = CarPark.IdPlace"
                    + " WHERE BusStop.IdPlace= ? AND CarPark.IdPlace= ? ) AND Idplace= ? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.setInt(2, id);
            pstmt.setInt(3, id);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    
    @Override
    public PreparedStatement beforeInsert(PlaceDTO data) {       
        try {
            String sql
                    = "INSERT INTO Place"
                    + " (Street, Town)"
                    + " VALUES(?,?)";
            PreparedStatement pstmt = null;
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getStreet());
            pstmt.setString(2, data.getTown());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

}
