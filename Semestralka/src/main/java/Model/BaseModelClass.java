/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BaseModelClass {

    public static boolean CloseStmt(ResultSet res, PreparedStatement stmt) {
        try {
            if (res != null && res.isClosed() == false) {
                res.close();
            }
            if (stmt != null && stmt.isClosed() == false) {
                stmt.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return false;
        }
    }

    public static boolean CloseStmt(PreparedStatement stmt) {
        try {
            if (stmt != null && stmt.isClosed() == false) {
                stmt.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return false;
        }
    }

    public static boolean CloseRes(ResultSet res) {
        try {
            if (res != null && res.isClosed() == false) {
                res.close();
            }
            return true;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return false;
        }
    }

}
