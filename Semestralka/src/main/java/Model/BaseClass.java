/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.DeleteReaction;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 * @param <T>
 */
public class BaseClass<T> {
    
    //start logging area
    public static final Logger LOG = Logger.getLogger("semestralkaModel.BaseClass");
    public static boolean isLogCreate = false;
     private void setLogging(){
         try{
        LOG.setLevel(Level.ALL);          
        FileHandler fh;           
        fh = new FileHandler("./Log/logFile.txt", true);       
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);    
        LOG.addHandler(fh);   
        isLogCreate=true;
        } catch (IOException ex) {
            Logger.getLogger(BaseClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(BaseClass.class.getName()).log(Level.SEVERE, null, ex);
        }            
        }
    //end logging area
 
    /**
     *  Hold connection
     */
    protected Connection conn;

    /**
     * Constructor, create connection
     */
    public BaseClass() {       
        DBConnection dbConnection = new DBConnection();
        conn = dbConnection.connect(); 
        if(!isLogCreate){
        setLogging();
    }
    }   
    
    /**
     * Method runs before method Insert
     * @param data
     * @return
     */
    public PreparedStatement beforeInsert(T data) {
        return null;
    }

    /**
     * Method Insert 
     * @param data
     * @return
     */
    public boolean Insert(T data) {
        try {            
            PreparedStatement pstmt = beforeInsert(data);
            pstmt.execute();
            return true;            
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            BaseClass.LOG.warning("insert Fail: "+ e.getMessage());
            return false;
        }
    }

    /**
     *Method runs before method update
     * @param data
     * @return
     */
    public PreparedStatement BeforeUpdate(T data) {
        return null;
    }

    /**
     *Method update
     * @param data
     * @return
     */
    public boolean Update(T data) {
        try {
            PreparedStatement pstmt = BeforeUpdate(data);
            pstmt.execute();
            BaseModelClass.CloseStmt(null, pstmt);
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            LOG.warning("update Fail: "+ e.getMessage() );
            return false;
        }
        return true;
    }

    /**
     *Method runs before method delete
     * @param data
     * @return
     */
    public PreparedStatement BeforeDelete(T data) {
        return null;
    }

    /**
     * Method delete
     * @param data
     * @return
     */
    public DeleteReaction Delete(T data) {
        try {
            PreparedStatement pstmt = BeforeDelete(data);
            pstmt.execute();
            BaseModelClass.CloseStmt(null, pstmt);
            PostDelete(data);
            return DeleteReaction.TRUE;
        } catch (SQLException e) {
            if(e.getSQLState().equals("23503")){
                System.out.print(e.getMessage());
                LOG.warning("Delete constraint error: "+ e.getMessage() );
                return DeleteReaction.CONSTRAINTERROR;
            } 
            System.out.print(e.getMessage());
            LOG.warning("Delete Fail: "+ e.getMessage() );
            return DeleteReaction.FALSE;            
        }
    }

    /**
     * Method runs before method getById
     * @param data
     * @return
     */
    public PreparedStatement BeforeGetById(T data) {
        return null;
    }

    /**
     * method get datatype from id 
     * @param data send datatype if id is from more than one atribute
     * @return
     */
    public T getById(T data) {
        try {
            PreparedStatement pstmt = BeforeGetById(data);
            ResultSet res = pstmt.executeQuery();
            res.next();
            return getData(res);
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            LOG.warning("Get item Fail: "+ e.getMessage() );
            return null;
        }
    }

    /**
     *Method runs before method ValidateUpdate
     * @param data
     * @return
     */
    public PreparedStatement BeforeValidateUpdate(T data) {
        return null;
    }

    /**
     *Method verify if you can update item
     * @param data
     * @return
     */
    public boolean ValidateUpdate(T data) {
        try {
            PreparedStatement pstmt = BeforeValidateUpdate(data);
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                BaseModelClass.CloseStmt(res, pstmt);
                BaseModelClass.CloseRes(res);
                return false;
            }
            BaseModelClass.CloseRes(res);
            BaseModelClass.CloseStmt(res, pstmt);
            return true;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            LOG.warning("Validate update Fail: "+ e.getMessage() );
            return false;
        }
    }

    /**
     *Method runs after method Delete
     * @param data
     * @return
     */
    public boolean PostDelete(T data) {
        return true;
    }

    /**
     *Method runs before method List
     * @param array
     * @return
     */
    public PreparedStatement BeforeList(Object array) {
        return null;
    }

    /**
     * Method get data from ResultSet to suitable datatype
     * @param res
     * @return
     */
    public T getData(ResultSet res) {
        return null;
    }

    /**
     * Method give List of datatypes
     * @param o
     * @return
     */
    public ArrayList<T> List(Object o) {
        try {
            ArrayList<T> array = new ArrayList<>();
            PreparedStatement pstmt = BeforeList(o);
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                array.add(getData(res));
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return array;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            LOG.warning("Get list Fail: "+ e.getMessage() );
            return null;
        }
    }
    
    /**
     * get id from different key in table in sql
     * @param sql
     * @param conn
     * @param getColumn
     * @return
     */
    public static int getIdInt(String sql, Connection conn, String getColumn) {
        int result = -1;
            try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet res = pstmt.executeQuery();            
            while (res.next()) {
                result = res.getInt(getColumn);
            }
            BaseModelClass.CloseStmt(res, pstmt);
            return result;
        } catch (SQLException e) {
            System.out.println(e);
            LOG.warning("Get id Fail: "+ e.getMessage() );
            return -1;
        } 
    }
}
