/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.UpdateSpecification;
import Dto.BusConnectionDTO;
import Dto.BusStopDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusConnectionModel extends BaseClass<BusConnectionDTO> {

    private int minusStartDay = 0;
    private int plusDay = 0;
    
    /**
     * get list of Bus Connection 
     * @param line
     * @param choice
     * @return
     */
    public ArrayList<BusConnectionDTO> getBusConnectionList(String line, int choice) {
        ArrayList array = new ArrayList();
        array.add(line);
        array.add(choice);
        return List((Object) array);
    }

    /**
     * get busConn id from different key
     * @param hours
     * @param minutes
     * @param BusType
     * @param Bus
     * @param line
     * @param updateSpec
     * @return
     */
    public ArrayList<BusConnectionDTO> getbusConnIdArray(int hours, int minutes, String BusType, String Bus, String line, UpdateSpecification updateSpec) {
        try {
            Time time = new Time(hours, minutes, 0);
            String sql = "SELECT idLineStartTime , Day FROM lineStartTime WHERE  bus= ? AND busType = ? AND line= ? AND time = ? ;";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, Bus);
            prepare.setString(2, BusType);
            prepare.setString(3, line);
            prepare.setTime(4, time);
            ResultSet res = prepare.executeQuery();
            ArrayList<BusConnectionDTO> result = new ArrayList<BusConnectionDTO>();
            while (res.next()) {
                BusConnectionDTO busConn = new BusConnectionDTO();
                busConn.setId(res.getInt("idLineStartTime"));
                busConn.setDay(res.getInt("Day"));
                result.add(busConn);
            }
            BaseModelClass.CloseRes(res);
            BaseModelClass.CloseStmt(res, prepare);
            return result;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
      /**
     * you get sum of times
     * @param time1
     * @param time2
     * @return 
     */
    public Time timeSum(Time time1, Time time2) {
        int totalHours = time1.getHours() + time2.getHours();
        int totalMinutes = time1.getMinutes() + time2.getMinutes();
        if (totalMinutes >= 60) {
            totalHours++;
            totalMinutes = totalMinutes % 60;
        }
        if(totalHours>24){         
         Double a = Math.ceil((totalHours / 24));           
         plusDay = a.intValue();
        }
        return new Time(totalHours, totalMinutes, 0);
    }   

    /**
     * you get time1 - time2 
     * @param time1
     * @param time2
     * @return
     */
    public Time timeSubstr(Time time1, Time time2) {
        int totalHours = time1.getHours() - time2.getHours();
        int totalMinutes = time1.getMinutes() - time2.getMinutes();
        if (totalMinutes < 0) {
            totalHours--;
            totalMinutes = totalMinutes + 60;
        }
        if (totalHours < 0) {
            int totalHoursAbsolute = Math.abs(totalHours);
            Double a = Math.ceil((totalHoursAbsolute / 24) + 1);           
            minusStartDay = a.intValue();
        }
        Time time = new Time(totalHours, totalMinutes, 0);
        return time;
    }    

    /**
     * get time of one line route
     * @param line
     * @return
     */
    public Time getRouteTime(String line) {
        try {
            String sql = "SELECT sum(timeFromPrevBusStop) FROM stopOnRoute WHERE line= ? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, line);
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                Time time = new Time(0, res.getInt("sum"), 0);
                BaseModelClass.CloseStmt(res, pstmt);
                BaseModelClass.CloseRes(res);
                return time;
            }
            BaseModelClass.CloseRes(res);
            BaseModelClass.CloseStmt(res, pstmt);
            return new Time(0, 0, 0);
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return new Time(0, 0, 0);
        }
    }

    /**
     *
     * @param day
     * @param line
     * @param bus
     * @param hours
     * @param minutes
     * @param prevIdList
     * @return
     */
    public boolean ValidateUpdateBusConn(int day, String line, String bus, int hours, int minutes, ArrayList<BusConnectionDTO> prevIdList) {
        BusConnectionDTO data = new BusConnectionDTO();
        Time time = new Time(hours, minutes, 0);
        data.setDay(day);
        data.setLine(line);
        data.setBus(bus);
        data.setTime(time);
        return SpecialValidateUpdate(data, prevIdList);
    }

    // necessary to have special validate update for this Entity
    private boolean SpecialValidateUpdate(BusConnectionDTO data, ArrayList<BusConnectionDTO> prevIdList) {
        try {
            //if you insert new BusConn
            boolean controlUpdate = (prevIdList!=null);
            
            data.setTimeOfRoute(getRouteTime(data.getLine()));
            // endTime is time of end route             
            Time endTime = timeSum(data.getTime(), data.getTimeOfRoute());
            //setting time             
            data.setDay(data.getDay() + plusDay);            
            if (data.getDay() > 7) {
              int day = data.getDay() % 7;
               if(day==0){
               day=7;
              }
              data.setDay(day);               
            }
            
            ArrayList<BusConnectionDTO> arrayTimeLine = getTimeAndLineArray(data);
            //find out for each line if has'nt some route with choosen bus in the same time as is our connection
            for (int i = 0; i < arrayTimeLine.size(); i++) {
                Time startTime = timeSubstr(data.getTime(), arrayTimeLine.get(i).getTimeOfRoute());                             
                int startDay = data.getDay() - minusStartDay;
                if (startDay < 0) {
                    startDay = startDay % 7;
                    if (startDay == 0) {
                        startDay = 7;
                    }
                }                 
                String sql ="";
                // if day of foreign connection start is bigger than day of connection, you must find out days separately to the end of week, and then from the start 
                if(startDay>data.getDay())
                {
                sql = "SELECT * FROM lineStartTime WHERE bus= ? AND (( Day BETWEEN ? AND 7 )OR ( Day BETWEEN 0 AND ?))  AND ( Time BETWEEN ? AND ? ) AND Line = ?;";
                }
                else{
                sql = "SELECT * FROM lineStartTime WHERE bus= ? AND ( Day BETWEEN ? AND ? ) AND ( Time BETWEEN ? AND ? ) AND Line = ?;";
                 }
                PreparedStatement pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, data.getBus());
                pstmt.setInt(2, startDay);
                pstmt.setInt(3, data.getDay());
                pstmt.setTime(4, startTime);
                pstmt.setTime(5, endTime);
                pstmt.setString(6, arrayTimeLine.get(i).getLine());
                ResultSet res = pstmt.executeQuery();
                if (res.next()) {                    
                    if(controlUpdate){
                    //check if you cannot insert busConn, because you find conection in the same time isnt same connection, which you want to update...    
                    for (int j = 0; j < prevIdList.size(); j++) {
                        if (prevIdList.get(j).getId() == res.getInt("IdLineStartTime")) {
                            return true;
                        }
                    }
                    }
                    BaseModelClass.CloseStmt(res, pstmt);
                    BaseModelClass.CloseRes(res);
                    return false;
                }
            }
            return true;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return false;
        }
    }
      

    /**
     * get time route in minutes to the line where bus have some BusConnections
     * @param busConn
     * @return
     */
    public ArrayList<BusConnectionDTO> getTimeAndLineArray(BusConnectionDTO busConn) {
        try {
            PreparedStatement pstmt;
            if(busConn==null){
             String sql = "(SELECT line,sum(timeFromPrevBusStop)"
                    + " FROM stopOnRoute "
                    + " WHERE line IN  "
                    + " (SELECT DISTINCT line FROM lineStartTime) "
                    + " GROUP BY LINE)";
             pstmt = conn.prepareStatement(sql);
            }
            else{
            String sql = "(SELECT line,sum(timeFromPrevBusStop)"
                    + " FROM stopOnRoute "
                    + " WHERE line IN  "
                    + " (SELECT DISTINCT line FROM lineStartTime WHERE bus= ? ) "
                    + " GROUP BY LINE)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, busConn.getBus());
            }
            ResultSet res = pstmt.executeQuery();
            ArrayList<BusConnectionDTO> array = new ArrayList<>();
            while (res.next()) {
                BusConnectionDTO busC = new BusConnectionDTO();
                busC.setLine(res.getString("line"));
                Time time = new Time(0, res.getInt("sum"), 0);
                busC.setTimeOfRoute(time);
                array.add(busC);
            }
            BaseModelClass.CloseRes(res);
            BaseModelClass.CloseStmt(res, pstmt);
            return array;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }

    }

    /**
     * 
     * @param bus
     * @param busType
     * @param line
     * @param day
     * @param hours
     * @param minutes
     */
    public void InsertBusConnection(String bus, String busType, String line, int day, int hours, int minutes) {
        Time time = new Time(hours, minutes, 0);
        BusConnectionDTO busCon = new BusConnectionDTO();
        busCon.setBus(bus);
        busCon.setBusType(busType);
        busCon.setDay(day);
        busCon.setLine(line);
        busCon.setTime(time);
        Insert(busCon);
    }

    /**
     *
     * @param id
     */
    public void DeleteBusConnection(int id) {
        BusConnectionDTO busConn = new BusConnectionDTO();
        busConn.setId(id);
        Delete(busConn);
    }
    /**
     *
     * @param bus
     * @param busType
     * @param line
     * @param day
     * @param hours
     * @param minutes
     * @param Id
     */
    public boolean UpdateBusConnection(String bus, String busType, String line, int day, int hours, int minutes, int Id) {
        Time time = new Time(hours, minutes, 0);
        BusConnectionDTO busCon = new BusConnectionDTO();
        busCon.setBus(bus);
        busCon.setBusType(busType);
        busCon.setDay(day);
        busCon.setLine(line);
        busCon.setTime(time);
        busCon.setId(Id);
       return Update(busCon);
    }
    @Override
    public PreparedStatement BeforeDelete(BusConnectionDTO busConn) {
        try {
            ResultSet res = null;
            PreparedStatement pstmt = null;
            String sql
                    = "DELETE FROM lineStartTime"
                    + " WHERE idLineStartTime = ? ;";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, busConn.getId());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement beforeInsert(BusConnectionDTO data) {
        try {
            String sql
                    = "INSERT INTO lineStartTime"
                    + " (bus, busType, day, line, time)"
                    + "VALUES (?,?,?,?,?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getBus());
            prepare.setString(2, data.getBusType());
            prepare.setInt(3, data.getDay());
            prepare.setString(4, data.getLine());
            prepare.setTime(5, data.getTime());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    @Override
    public PreparedStatement BeforeUpdate(BusConnectionDTO data) {
        try {
            String sql = "UPDATE lineStartTime"
                    + " SET  bus= ? , busType = ? , day= ? , line= ? , time = ?"
                    + " WHERE idLineStartTime = ?";

            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getBus());
            prepare.setString(2, data.getBusType());
            prepare.setInt(3, data.getDay());
            prepare.setString(4, data.getLine());
            prepare.setTime(5, data.getTime());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    @Override
    public PreparedStatement BeforeList(Object o) {
        try {
            ArrayList array = (ArrayList) o;
            String line = (String) array.get(0);
            int choice = (int) array.get(1);
            String sql = "SELECT * FROM LineStartTime WHERE Line= ? ";
            if (choice != 0) {
                sql += " AND Day = ? ";
            }
            sql += " ORDER BY Day, Time ASC;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, line);
            if (choice != 0) {
                pstmt.setInt(2, choice);
            }
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    @Override
    public BusConnectionDTO getData(ResultSet res) {
        try {
            BusConnectionDTO busConn = new BusConnectionDTO();
            busConn.setBus(res.getString("Bus"));
            busConn.setBusType(res.getString("BusType"));
            busConn.setDay(res.getInt("Day"));
            busConn.setId(res.getInt("IdLineStartTime"));
            busConn.setLine(res.getString("Line"));
            busConn.setTime(res.getTime("Time"));
            return busConn;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    
    public int getPlusDay() {
       int a = plusDay;
        plusDay = 0;        
        return a;
    }   
    
    
    public int getMinusStartDay() {
        int a = plusDay;
        plusDay = 0;   
        return a;
    }
}
