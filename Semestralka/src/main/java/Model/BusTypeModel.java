/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.DeleteReaction;
import Dto.BusStopDTO;
import Dto.BusTypeDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusTypeModel extends BaseClass<BusTypeDTO> {
    /**
     *
     * @param text
     * @return
     */
    public ArrayList<BusTypeDTO> getBusTypeList(String text) {
        ArrayList array = new ArrayList();
        array.add(text);
        return List((Object) array);
    }

    /**
     * generate name from parameters
     * @param Lowfloor
     * @param toilet
     * @param capacity
     * @param StewardsPlace
     * @return
     */
    public String generateName(boolean Lowfloor, boolean toilet, String capacity, String StewardsPlace) {
        String generateName = "";
        if (Lowfloor) {
            generateName += "N";
        }
        if (toilet) {
            generateName += "T";
        }
        generateName += "k" + capacity;
        generateName += "s" + StewardsPlace;
        return generateName;
    }

    /**
     *
     * @param IdName
     * @return
     */
    public DeleteReaction DeleteBusType(String IdName) {
        BusTypeDTO busType = new BusTypeDTO();
        busType.setIdName(IdName);
        return Delete(busType);
    }

    @Override
    public BusTypeDTO getData(ResultSet res) {
        try {
            BusTypeDTO busType = new BusTypeDTO();
            busType.setIdName(res.getString("IdName"));
            busType.setCapacity(res.getInt("Capacity"));
            busType.setLowFloor(res.getBoolean("LowFloor"));
            busType.setStewardsPlace(res.getInt("StewardsPlace"));
            busType.setToilet(res.getBoolean("Toilet"));
            return busType;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeList(Object o) {
        try {
            ArrayList array = (ArrayList) o;
            String text = (String) array.get(0);
            String sql = "SELECT * FROM BusType WHERE IdName LIKE '%" + text + "%';";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeDelete(BusTypeDTO busType) {
        try {
            ResultSet res = null;
            PreparedStatement pstmt = null;
            String sql
                    = "DELETE FROM BusType"
                    + " WHERE IdName = ? ;";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, busType.getIdName());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeValidateUpdate(BusTypeDTO data) {
        try {
            String sql = "SELECT * FROM BusType WHERE IdName = ? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getIdName());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement beforeInsert(BusTypeDTO data) {
        try {
            String sql
                    = "INSERT INTO BusType"
                    + " (IdName, LowFloor, Capacity, Toilet, StewardsPlace)"
                    + "VALUES (?,?,?,?,?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getIdName());
            prepare.setBoolean(2, data.isLowFloor());
            prepare.setInt(3, data.getCapacity());
            prepare.setBoolean(4, data.isToilet());
            prepare.setInt(5, data.getStewardsPlace());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeUpdate(BusTypeDTO data) {
        try {
            String sql = "UPDATE BusType"
                    + " SET IdName = ? , capacity = ? , StewardsPlace = ?, Toilet = ? , LowFloor = ?  "
                    + " WHERE IdName = ? ";

            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getIdName());
            prepare.setInt(2, data.getCapacity());
            prepare.setInt(3, data.getStewardsPlace());
            prepare.setBoolean(4, data.isToilet());
            prepare.setBoolean(5, data.isLowFloor());
            prepare.setString(6, data.getPreviousIdName());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

}
