/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.DeleteReaction;
import Dto.BusStopDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusStopModel extends BaseClass<BusStopDTO> {

    private PlaceModel place;

    /**
     * use placeModel
     */
    public BusStopModel() {
        super();
        place = new PlaceModel();
    }

    /**
     *
     * @param indication
     * @param street
     * @param town
     */
    public void InsertBusStop(String indication, String street, String town) {
        BusStopDTO busStop = new BusStopDTO();
        busStop.setSignification(indication);
        busStop.setStreet(street);
        busStop.setTown(town);
        Insert(busStop);
    }

    /**
     *
     * @param signification
     * @param street
     * @param town
     */
    public void UpdateBusStop(String signification, String street, String town) {
        BusStopDTO busStop = new BusStopDTO();
        busStop.setSignification(signification);
        busStop.setStreet(street);
        busStop.setTown(town);
        Update(busStop);
    }

    /**
     *
     * @param choice
     * @param text
     * @return
     */
    public ArrayList<BusStopDTO> getBusStopList(int choice, String text) {
        ArrayList array = new ArrayList();
        array.add(choice);
        array.add(text);
        return List((Object) array);
    }

    /**
     *
     * @param indication
     * @return
     */
    public boolean ValidateBusStop(String indication) {
        BusStopDTO busStop = new BusStopDTO();
        busStop.setSignification(indication);
        return ValidateUpdate(busStop);
    }

    /**
     *
     * @param signific
     * @param street
     * @param town
     * @return 
     */
    public DeleteReaction DeleteBusStop(String signific, String street, String town) {
        BusStopDTO busStop = new BusStopDTO();
        busStop.setSignification(signific);
        busStop.setStreet(street);
        busStop.setTown(town);
        return Delete(busStop);
    }

    /**
     *
     * @param signific
     * @param street
     * @param town
     * @return
     */
    public BusStopDTO getBusStop(String signific, String street, String town) {
        BusStopDTO busStop = new BusStopDTO();
        busStop.setSignification(signific);
        busStop.setStreet(street);
        busStop.setTown(town);
        return getById(busStop);
    }

    @Override
    public PreparedStatement BeforeValidateUpdate(BusStopDTO data) {
        try {
            String sql = "SELECT * FROM BusStop WHERE signification=? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getSignification());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement beforeInsert(BusStopDTO data) {
        try {
            int idPlace = place.getPlaceId(data.getStreet(), data.getTown());
            if (idPlace < 0) {
                place.InsertPlace(data.getStreet(), data.getTown());
                idPlace = place.getPlaceId(data.getStreet(), data.getTown());
            }
            String sql
                    = "INSERT INTO BusStop"
                    + " (signification, idPlace)"
                    + "VALUES (?,?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getSignification());
            prepare.setInt(2, idPlace);
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeUpdate(BusStopDTO data) {
        try {
            int idPlace = place.getPlaceId(data.getStreet(), data.getTown());

            String sql = "UPDATE BusStop"
                    + " SET signification = ?"
                    + " WHERE idPlace =?";

            PreparedStatement prepare = conn.prepareStatement(sql);

            prepare.setString(1, data.getSignification());
            prepare.setInt(2, idPlace);
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public BusStopDTO getData(ResultSet res) {
        try {
            BusStopDTO busStop = new BusStopDTO();
            busStop.setStreet(res.getString("street"));
            busStop.setTown(res.getString("town"));
            busStop.setSignification(res.getString("signification"));
            return busStop;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeList(Object o) {
        try {
            ArrayList array = (ArrayList) o;
            int choice = (int) array.get(0);
            String text = (String) array.get(1);
            String filter;
            switch (choice) {
                case 1:
                    filter = "Street";
                    break;
                case 2:
                    filter = "Town";
                    break;
                default:
                    filter = "Signification";
                    break;
            }
            String sql = "SELECT signification,town,street FROM BusStop NATURAL JOIN Place WHERE " + filter + " LIKE '%" + text + "%';";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeDelete(BusStopDTO busStop) {
        try {
            ResultSet res = null;
            PreparedStatement pstmt = null;
            String sql
                    = "DELETE FROM BusStop"
                    + " WHERE signification = ? ;";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, busStop.getSignification());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean PostDelete(BusStopDTO busStop) {
        place.DeletePlace(busStop.getStreet(), busStop.getTown());
        return true;
    }

    @Override
    public PreparedStatement BeforeGetById(BusStopDTO busStop) {
        try {
            ResultSet res = null;
            PreparedStatement pstmt = null;
            String sql
                    = "SELECT * FROM BusStop NATURAL JOIN Place "
                    + " WHERE signification = ? ;";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, busStop.getSignification());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

}
