/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Dto.BusStopDTO;
import Dto.CarParkDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class CarParkModel extends BaseClass<CarParkDTO> {

    private PlaceModel place;

    /**
     * using placeModel
     */
    public CarParkModel() {
        super();
        place = new PlaceModel();
    }

    /**
     *
     * @param indication
     * @param capacity
     * @return
     */
    public boolean ValidateUpdateCarPark(String indication, int capacity) {
        CarParkDTO carPark = new CarParkDTO();
        carPark.setSignification(indication);
        carPark.setCapacity(capacity);        
        return ValidateUpdate(carPark);
    }
    
    /**
     *
     * @param indication
     * @param street
     * @param town
     * @param capacity
     */
    public void InsertCarPark(String indication, String street, String town, int capacity) {
        CarParkDTO carPark = new CarParkDTO();
        carPark.setSignification(indication);
        carPark.setStreet(street);
        carPark.setTown(town);
        carPark.setCapacity(capacity);
        Insert(carPark);
    }

    /**
     * get count of bus, which are in the CarPark.
     * @param signification
     * @return
     */
    public int getOccupiedCapacity(String signification){
        try{
     String sql = " SELECT COUNT(*) as occupied FROM Bus where carpark = ? ";
     PreparedStatement pstmt = conn.prepareStatement(sql);
     pstmt.setString(1, signification);
     ResultSet res = pstmt.executeQuery();
     int result=0;
     if(res.next()){
       result= res.getInt("occupied");
     }
    return result;
     }
        catch(SQLException e){
        System.out.print(e.getMessage());
        return 0;
        }
    }
     /**
     *
     * @param signification
     * @param street
     * @param town
     * @param capacity
     */
    public void UpdateCarPark(String signification, String street, String town, int capacity) {
        CarParkDTO carPark = new CarParkDTO();
        carPark.setSignification(signification);
        carPark.setStreet(street);
        carPark.setTown(town);
        carPark.setCapacity(capacity);
        Update(carPark);
    }
    /**
     *
     * @param signific
     * @param street
     * @param town
     */
    public void DeleteCarPark(String signific, String street, String town) {
        CarParkDTO carPark = new CarParkDTO();
        carPark.setSignification(signific);
        carPark.setStreet(street);
        carPark.setTown(town);
        Delete(carPark);
    }
    /**
     *
     * @param choice
     * @param text
     * @return
     */
    public ArrayList<CarParkDTO> getCarParkList(int choice, String text) {
        ArrayList array = new ArrayList();
        array.add(choice);
        array.add(text);
        ArrayList<CarParkDTO> carParks = List((Object) array);
        //set free Capacity 
        for(CarParkDTO carPark: carParks){
        int freeCap = carPark.getCapacity() - getOccupiedCapacity(carPark.getSignification());
        carPark.setFreeCapacity(freeCap);
        }
        return  carParks;
    }

    @Override
    public PreparedStatement beforeInsert(CarParkDTO data) {
        try {
            int idPlace = place.getPlaceId(data.getStreet(), data.getTown());
            if (idPlace < 0) {
                place.InsertPlace(data.getStreet(), data.getTown());
                idPlace = place.getPlaceId(data.getStreet(), data.getTown());
            }
            String sql
                    = "INSERT INTO CarPark"
                    + " (signification, idPlace, capacity)"
                    + "VALUES (?,?,?)";
            PreparedStatement prepare = conn.prepareStatement(sql);
            prepare.setString(1, data.getSignification());
            prepare.setInt(2, idPlace);
            prepare.setInt(3, data.getCapacity());
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeValidateUpdate(CarParkDTO data) {
        try {
            String sql = "SELECT * FROM CarPark  WHERE signification=? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getSignification());      
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
        @Override
    public PreparedStatement BeforeGetById(CarParkDTO data) {
        try {
            String sql = "SELECT * FROM CarPark NATURAL JOIN Place WHERE signification=? ;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, data.getSignification());      
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
    @Override
    public CarParkDTO getData(ResultSet res) {
        try {
            CarParkDTO carPark = new CarParkDTO();
            carPark.setStreet(res.getString("street"));
            carPark.setTown(res.getString("town"));
            carPark.setSignification(res.getString("signification"));
            carPark.setCapacity(Integer.parseInt(res.getString("Capacity")));
            return carPark;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeDelete(CarParkDTO carPark) {
        try {
            ResultSet res = null;
            PreparedStatement pstmt = null;
            String sql
                    = "DELETE FROM CarPark"
                    + " WHERE signification = ? ;";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, carPark.getSignification());
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean PostDelete(CarParkDTO carPark) {
        place.DeletePlace(carPark.getStreet(), carPark.getTown());
        return true;
    }
    @Override
    public PreparedStatement BeforeList(Object o) {
        try {
            ArrayList array = (ArrayList) o;
            int choice = (int) array.get(0);
            String text = (String) array.get(1);
            String filter;
            switch (choice) {
                case 1:
                    filter = "Street";
                    break;
                case 2:
                    filter = "Town";
                    break;
                default:
                    filter = "Signification";
                    break;
            }
            String sql = "SELECT signification,town,street, Capacity FROM CarPark NATURAL JOIN Place WHERE " + filter + " LIKE '%" + text + "%';";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            return pstmt;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return null;
        }
    }

    @Override
    public PreparedStatement BeforeUpdate(CarParkDTO data) {
        try {
            int idPlace = place.getPlaceId(data.getStreet(), data.getTown());

            String sql = "UPDATE CarPark"
                    + " SET signification = ? , capacity = ? "
                    + " WHERE idPlace =?";

            PreparedStatement prepare = conn.prepareStatement(sql);

            prepare.setString(1, data.getSignification());
            prepare.setInt(2, data.getCapacity());
            prepare.setInt(3, idPlace);
            return prepare;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return null;
        }
    }
}
