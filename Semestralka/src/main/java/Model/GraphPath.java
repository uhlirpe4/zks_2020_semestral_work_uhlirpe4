/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Enums.Order;
import Dto.BusConnectionDTO;
import Dto.CurrentSituationDTO;
import Dto.LineDTO;
import Dto.StopOnRouteDTO;
import java.sql.Time;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class GraphPath {
    private CurrentSituationModel currentSituationModel;
    private PrintModel printModel;
    private LineModel lineModel;
    private BusConnectionModel busConnectionModel;

    /**
     * constructor
     */
    public GraphPath() {
        currentSituationModel = new CurrentSituationModel();
        printModel = new PrintModel();
        lineModel = new LineModel();
        busConnectionModel = new BusConnectionModel();
    }

    private ArrayList<CurrentSituationDTO> FindBestConnectionsList(String StartBusStop, String FinishBusStop, int day, int hours, int minutes) {
        Queue<String> BusStopsQueue = new LinkedList<String>();
        BusStopsQueue.add(StartBusStop);
        Time selectedTime = new Time(hours, minutes, 0);
        // list of best connections to busStop and selected time 
        ArrayList<CurrentSituationDTO> findedBusConnections = new ArrayList<>();
        //add startBusStop set to Array
        CurrentSituationDTO currentSit = new CurrentSituationDTO();
        currentSit.setActualBusStop(StartBusStop);
        currentSit.setDay(day);
        currentSit.setTimeArrived(selectedTime);        
        findedBusConnections.add(currentSit);
        
        while (!BusStopsQueue.isEmpty()) {
            boolean isNewBusStop = true;
            String actualBusStop = BusStopsQueue.remove();
            //find the timefor which we look for the best connection || cas ke kteremu hledat nejlepsi spoj
            for (CurrentSituationDTO curSit : findedBusConnections) {
                if (curSit.getActualBusStop().equals(actualBusStop)) {
                    selectedTime = curSit.getTimeArrived();
                }
            }
            ArrayList<LineDTO> lineList = printModel.getListLinesOnBusStop(actualBusStop);
            for (LineDTO line : lineList) {
                //get all busConnections line with time on actual busStop ||  dostanu všechny spoje linky a zastávky 
                ArrayList<BusConnectionDTO> LineBusStopArrayConnections = printModel.getReportLineBusConnections(line.getNumber(), actualBusStop,Order.DAYTIME);
                //find best Connection to our time || najde nejbližší spojení k danému času
                BusConnectionDTO busConn = getBestConnection(LineBusStopArrayConnections, selectedTime, day);
                //find next busStop and count time of arrival ||  vypočítej v jaký čas bude na další zastávce a jaká je to zastávka.
                CurrentSituationDTO currentSituation = getNextBusStopSituation(busConn, line.getNumber(), actualBusStop);
                //if isnt next busStop => busStop is terminus and connection to next bus doesnt exist. ||  null, je to na konečné, tam už nic nebude=> ppřeskočit
                if (currentSituation == null) {
                    continue;
                }
                //We will look if actual currentSituation is best what we found yet || projeď pole výsledků a zjisti, jestli tam není s menším časem, pokud ano, continue, jinak přeulož
               for (CurrentSituationDTO item : findedBusConnections) {
                    //check if situation is on the busStop early than other  ||kontrola jestli spoj je na stejne zastavce dříve, než máme uloženo
                    if (item.getActualBusStop().equals(currentSituation.getActualBusStop())
                            && currentSituation.getTimeArrived().before(item.getTimeArrived())
                            && currentSituation.getDay() <= item.getDay()) {
                        findedBusConnections.remove(item);
                        findedBusConnections.add(currentSituation);
                        BusStopsQueue.add(currentSituation.getActualBusStop());
                        isNewBusStop = false;
                        break;
                    }                   
                    //when currentSituation isnt the best of founded || kdyz je ale stejna zastavka, ale nebyla lepsi 
                    else if (item.getActualBusStop().equals(currentSituation.getActualBusStop())) {
                        isNewBusStop = false;
                        break;
                    }
                }
                if (isNewBusStop) {                    
                   findedBusConnections.add(currentSituation);
                   BusStopsQueue.add(currentSituation.getActualBusStop());
                }
            }
        }
        return findedBusConnections;
    }   
    private CurrentSituationDTO getNextBusStopSituation(BusConnectionDTO busConn, String choosenLine, String curBusStop) {
        ArrayList<StopOnRouteDTO> stops = lineModel.getStopsOnRouteList(choosenLine);
        StopOnRouteDTO nextStopOnRoute = lineModel.getNextStopOnRoute(choosenLine, curBusStop);
        if(nextStopOnRoute==null){
        return null;
        }
        String nextBusStop = nextStopOnRoute.getBusStop();
        //actualize time on next busStop
        Time addTime = printModel.sumTimeFromPrevBusStop(stops, nextBusStop);
        busConn = printModel.ActualizeTimeToBusStop(busConn, addTime);
        CurrentSituationDTO curSit = new CurrentSituationDTO();
        // set Current Situation
        curSit.setActualBusStop(nextBusStop);
        curSit.setLine(choosenLine);
        curSit.setTimeArrived(busConn.getTime());
        curSit.setDay(busConn.getDay());
        return curSit;
    }

    /**
     * function return string of best way how to get from StartBusStop to FinishBusStop
     * @param StartBusStop
     * @param FinishBusStop
     * @param day
     * @param hours
     * @param minutes
     * @return
     */
    public String findPath(String StartBusStop, String FinishBusStop, int day, int hours, int minutes) {
        ArrayList<CurrentSituationDTO> bestConnectionsArr = FindBestConnectionsList(StartBusStop, FinishBusStop, day, hours, minutes);        
        SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>  
        graph  = setGraph(StartBusStop,bestConnectionsArr);      
        String pathTextInfo;
        //check if exists connections betwen start and finish || kontrola jestli existují spojení mezi startem a cílem 
        if (graph.containsVertex(StartBusStop) == false || graph.containsVertex(FinishBusStop) == false) {
            pathTextInfo = "Žádné spojení mezi těmito zastávkami neexistuje";
        } else {
            //find shortest path || vypočti nejkratší trasu 
            org.jgrapht.GraphPath<String, DefaultWeightedEdge> path = DijkstraShortestPath.findPathBetween(graph, StartBusStop, FinishBusStop);
            List<DefaultWeightedEdge> edgesList = path.getEdgeList();
            List<String> vertexesList = path.getVertexList();
            // zjistit linky a přestupy
            pathTextInfo = findLinesTransfers(bestConnectionsArr, edgesList, vertexesList);            
        }
        return pathTextInfo;
    }
    
    private SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> setGraph( String StartBusStop, ArrayList<CurrentSituationDTO> bestConnectionsArr){
         SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>  graph = new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>
            (DefaultWeightedEdge.class); 
        // add vertexes
        graph.addVertex(StartBusStop);
        for (CurrentSituationDTO bestConn : bestConnectionsArr) {
            graph.addVertex(bestConn.getActualBusStop());
        }
        // add edges
        for (CurrentSituationDTO bestConn : bestConnectionsArr) {
            StopOnRouteDTO stopOnRoute = lineModel.getPrevStopOnRoute(bestConn.getLine(), bestConn.getActualBusStop());
            if(stopOnRoute == null){
            continue;
            }
            String prevBusStop = stopOnRoute.getBusStop();
            CurrentSituationDTO prevBusStopSit = getCurrSituationFromArray(prevBusStop, bestConnectionsArr);          
            //if graph doesnt contain prev busStop you cant do edge || pokud není předchozí zastávka, tak není možné udělat hranu.
            if (!graph.containsVertex(prevBusStop)) {
                continue;
            }            
            DefaultWeightedEdge e1 = graph.addEdge(prevBusStop, bestConn.getActualBusStop());
            //find weight of edge || zjisti hodnotu hrany a pridej do grafu  
            int weight = getEdgeWeight(prevBusStopSit, bestConn);
            graph.setEdgeWeight(e1, weight);
        }
        return graph;
    }
    
    private String findLinesTransfers(ArrayList<CurrentSituationDTO> vertexesArray, List<DefaultWeightedEdge> edgesList, List<String> vertexesList) {
        String information;
        if (edgesList.isEmpty()) {
            return "Jste na Místě";
        }
        information = "Začátek na zastávce " + vertexesList.get(0) + " v čase " + getTimeFromArray(vertexesArray, vertexesList.get(0))+"\n";
        // find times and transfers ||  zjistit casy a prestupy   
        String line = "";
        String prevLine = "";
        for (int i = 1; i < vertexesList.size(); i++) {    
            //find line of vertex || zjistit linku 
            line = getLineFromArray(vertexesArray, vertexesList.get(i));
            if (!line.equals(prevLine)) {
                int waitingMinutes = 0;
                if(waitingMinutes!= 0){
                     information += "počkejte " +waitingMinutes+  " minut";
                }
                information += " Na zastávce " + vertexesList.get(i-1)
                        + " nastupte na linku " + line + " v čase " + getTimeOnPrevBusStop(vertexesArray, vertexesList.get(i))+ " \n";
            }
            prevLine = line;
        }
        return information;
    }

    private String getTimeOnPrevBusStop(ArrayList<CurrentSituationDTO> vertexesArray, String busStop) {
        for (int i=0; i< vertexesArray.size();i++) {
            String vertexBusStop = vertexesArray.get(i).getActualBusStop();
            if (busStop.equals(vertexBusStop)) {
                StopOnRouteDTO prevStoponRoute = lineModel.getStopOnRoute(vertexesArray.get(i).getLine(), busStop);
                Time time = busConnectionModel.timeSubstr(vertexesArray.get(i).getTimeArrived(),new Time(0,prevStoponRoute.getTimeFromPrevBusStop(),0));
                return time.getHours()+":"+time.getMinutes() ;
            }
        }
        return "";
    }
  private String getTimeFromArray(ArrayList<CurrentSituationDTO> vertexesArray, String busStop) {
        for (int i=0; i< vertexesArray.size();i++) {
            String vertexBusStop = vertexesArray.get(i).getActualBusStop();
            if (busStop.equals(vertexBusStop)) {
               
                return vertexesArray.get(i).getTimeArrived().getHours()+":"+vertexesArray.get(i).getTimeArrived().getMinutes() ;
            }
        }
        return "";
    }
    
    private int getTimeDistraction(String line, String busStop, double edgeWeight) {
        ArrayList<StopOnRouteDTO> stops = lineModel.getStopsOnRouteList(line);
        StopOnRouteDTO stop = getStopOnRouteFromArray(stops, busStop);
        int waitingMinutes = 0;
        if (stop != null) {
            waitingMinutes = (int) (edgeWeight - ((int) stop.getTimeFromPrevBusStop()));
        }
        return waitingMinutes;
    }
    private StopOnRouteDTO getStopOnRouteFromArray(ArrayList<StopOnRouteDTO> stops, String busStop) {
        for (StopOnRouteDTO stop : stops) {
            if (stop.getBusStop().equals(busStop)) {
                return stop;
            }
        }
        return null;
    }

    private String getLineFromArray(ArrayList<CurrentSituationDTO> vertexesArray, String busStop) {
        for (CurrentSituationDTO vertex : vertexesArray) {
            if (vertex.getActualBusStop().equals(busStop)) {
                return vertex.getLine();
            }
        }
        return "";
    }

    private CurrentSituationDTO getCurrSituationFromArray(String busStop, ArrayList<CurrentSituationDTO> bestConnectionsArr) {
        for (CurrentSituationDTO bestConn : bestConnectionsArr) {
            if (bestConn.getActualBusStop().equals(busStop)) {
                return bestConn;
            }
        }
        return null;
    }
    // get weight of edge in graph (weight== minutes)
    private int getEdgeWeight(CurrentSituationDTO busStopPreviouse, CurrentSituationDTO busStopArrive) {
        Time time = busConnectionModel.timeSubstr(busStopArrive.getTimeArrived(), busStopPreviouse.getTimeArrived());
        int days = busConnectionModel.getMinusStartDay();
        int daysMinutes = 0;
        if (days < 0) {
            daysMinutes = (days - 1) * 24 * 60;
        }
        int resultTime = time.getHours() + time.getMinutes();        
        return daysMinutes + resultTime;
    }

    //find best Connection to our time || najde nejbližší spojení k danému času
    private BusConnectionDTO getBestConnection(ArrayList<BusConnectionDTO> LineBusStopArrayConnections, Time selectedTime, int day) {
        //order by DAY TIME
        for (BusConnectionDTO busConn : LineBusStopArrayConnections) {
            if (day > busConn.getDay()) {
                continue;
            }
            if ((busConn.getTime().after(selectedTime) && day == busConn.getDay()) || (busConn.getTime().equals(selectedTime) && day == busConn.getDay())) {
                return busConn;
            }
            if (day < busConn.getDay()) {
                return busConn;
            }
        }
        if (LineBusStopArrayConnections.size() > 0) {
            LineBusStopArrayConnections.get(0).setDay( LineBusStopArrayConnections.get(0).getDay()+7);
            return LineBusStopArrayConnections.get(0);
        }
        return null;
    }
}
