/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.awt.Frame;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class DBConnection {
    
    public DBConnection() {
        /*
    Osobní číslo: 5622
    Databáze: db18_uhlirpe4
    Login: db18_uhlirpe4
    Heslo: Md52Xx
        
    jdbc:postgresql://<database_host>:<port>/<database_name>
    jdbc:postgresql://host+port/db18_uhlirpe4    
         */
    }

    public Connection connect() {
        Connection conn = null;
        String host = "localhost";
        String port = ":5432";
        String username = "postgres";
        String password = "kaliste23";
        String jdbcCon = "jdbc:postgresql://" + host + port + "/postgres";
        try {
            conn = DriverManager.getConnection(jdbcCon, username, password);
          //  System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {           
           if(e.getSQLState().equals("08001"))
           {
             Frame frame = new JFrame();
             JOptionPane.showMessageDialog(frame, "Nelze se připojit k DB. Zkontrolujte své připojení k síti.","Varování",
             JOptionPane.WARNING_MESSAGE);
           }
            System.out.println(e.getMessage());
        }
        return conn;
    }

}
