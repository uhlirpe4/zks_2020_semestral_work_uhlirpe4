/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class StopOnRouteDTO {

    private int id;
    private String line;
    private String busStop;
    private int busStopNumber;
    private int timeFromPrevBusStop;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getBusStop() {
        return busStop;
    }

    public void setBusStop(String busStop) {
        this.busStop = busStop;
    }

    public int getBusStopNumber() {
        return busStopNumber;
    }

    public void setBusStopNumber(int busStopNumber) {
        this.busStopNumber = busStopNumber;
    }

    public int getTimeFromPrevBusStop() {
        return timeFromPrevBusStop;
    }

    public void setTimeFromPrevBusStop(int timeFromPrevBusStop) {
        this.timeFromPrevBusStop = timeFromPrevBusStop;
    }

}
