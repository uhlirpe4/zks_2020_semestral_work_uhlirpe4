/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class LineDTO {

    private String number;
    private String firstBusStop;
    private String lastBusStop;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFirstBusStop() {
        return firstBusStop;
    }

    public void setFirstBusStop(String firstBusStop) {
        this.firstBusStop = firstBusStop;
    }

    public String getLastBusStop() {
        return lastBusStop;
    }

    public void setLastBusStop(String lastBusStop) {
        this.lastBusStop = lastBusStop;
    }
}
