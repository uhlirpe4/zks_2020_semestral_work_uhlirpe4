/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.sql.Time;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusConnectionDTO {
    private int id; 
    private String line;
    private int day;
    private String bus;
    private String busType;
    private Time time;
    private Time timeOfRoute;

    public Time getTimeOfRoute() {
        return timeOfRoute;
    }

    public void setTimeOfRoute(Time timeOfRoute) {
        this.timeOfRoute = timeOfRoute;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
