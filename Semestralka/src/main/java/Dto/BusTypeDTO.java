/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusTypeDTO {
    private String IdName;
    private boolean lowFloor;
    private int capacity;
    private int stewardsPlace;

    public String getPreviousIdName() {
        return previousIdName;
    }

    public void setPreviousIdName(String previousIdName) {
        this.previousIdName = previousIdName;
    }
    private boolean toilet;
    private String previousIdName;

    public String getIdName() {
        return IdName;
    }

    public void setIdName(String IdName) {
        this.IdName = IdName;
    }
    
    public boolean isLowFloor() {
        return lowFloor;
    }

    public void setLowFloor(boolean lowFloor) {
        this.lowFloor = lowFloor;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getStewardsPlace() {
        return stewardsPlace;
    }

    public void setStewardsPlace(int stewardsPlace) {
        this.stewardsPlace = stewardsPlace;
    }

    public boolean isToilet() {
        return toilet;
    }

    public void setToilet(boolean toilet) {
        this.toilet = toilet;
    }
    
}
