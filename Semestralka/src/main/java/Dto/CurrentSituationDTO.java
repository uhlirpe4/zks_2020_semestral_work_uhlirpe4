/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.sql.Time;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class CurrentSituationDTO {
    private String line;
    private String nextBusStop;
    private String actualBusStop;    
    private String bus;
    private Time timeArrived;
    private int Day;

    public int getDay() {
        return Day;
    }

    public void setDay(int Day) {
        this.Day = Day;
    }
    
    
public String getActualBusStop() {
        return actualBusStop;
    }

    public void setActualBusStop(String actualBusStop) {
        this.actualBusStop = actualBusStop;
    }
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getNextBusStop() {
        return nextBusStop;
    }

    public void setNextBusStop(String nextBusStop) {
        this.nextBusStop = nextBusStop;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

  

    public Time getTimeArrived() {
        return timeArrived;
    }

    public void setTimeArrived(Time timeArrived) {
        this.timeArrived = timeArrived;
    }
}
