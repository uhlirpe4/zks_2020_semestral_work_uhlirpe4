/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelTests;

import Enums.UpdateSpecification;
import Dto.BusConnectionDTO;
import Dto.StopOnRouteDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import static org.hamcrest.CoreMatchers.hasItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import Model.BusConnectionModel;
import junit.framework.Assert;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusConnectionModelTest {
    
    BusConnectionModel busConnModel;
    
    public BusConnectionModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {   
    }
    
    @AfterClass
    public static void tearDownClass() {
       
    }
    
    @Before
    public void setUp() {
        busConnModel = new BusConnectionModel();
    }
    
    @After
    public void tearDown() {
    }  

    /**
     * Test of timeSum method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testTimeSum() {
        Time time1 = new Time(21,25,0);
        Time time2 = new Time(1,25,0);
        Time expected = new Time(22,50,0);
        Time result = busConnModel.timeSum(time1, time2);
        assertEquals(expected, result);        
    }

    /**
     * Test of timeSubstr method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testTimeSubstr() {
        //arrange
        Time time1 = new Time(21,25,0);
        Time time2 = new Time(1,25,0);
        Time expected = new Time(20,00,0);
        //act
        Time result = busConnModel.timeSubstr(time1, time2);
        //assert
        assertEquals(expected, result); 
    }

    /**
     * Test of getRouteTime method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testGetRouteTime() {       
       //arrange
       Time expectedTime = new Time(0,10,0);
       //act
       Time resultTime = busConnModel.getRouteTime("TESTline");
       //assert
       assertEquals(expectedTime, resultTime);       
    } 

    /**
     * Test of BeforeDelete method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testBeforeDelete() {
        BusConnectionDTO busConn = new BusConnectionDTO();
        busConn.setId(1);
        PreparedStatement result = busConnModel.BeforeDelete(busConn);
        String expResult = "DELETE FROM lineStartTime"
                    + " WHERE idLineStartTime = 1 ";
        assertEquals(expResult, result.toString());
    } 

    /**
     * Test of beforeInsert method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testBeforeInsert() {
        BusConnectionDTO busConn = new BusConnectionDTO();
        busConn.setBus("bus");
        busConn.setBusType("busType");
        busConn.setDay(0);
        busConn.setLine("line");
        busConn.setTime(new Time(0,20,0));
        PreparedStatement result = busConnModel.beforeInsert(busConn);
        String expResult = "INSERT INTO lineStartTime"
                    + " (bus, busType, day, line, time)"
                    + "VALUES ('"+busConn.getBus()+"','"+busConn.getBusType()+"',"+busConn.getDay()+",'"+busConn.getLine()+"','00:20:00+01')";
        assertEquals(expResult, result.toString());
    }

    /**
     * Test of BeforeUpdate method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testBeforeUpdate() {
        BusConnectionDTO busConn = new BusConnectionDTO();
        busConn.setBus("bus");
        busConn.setBusType("busType");
        busConn.setDay(0);
        busConn.setLine("line");
        busConn.setTime(new Time(0,20,0));
        PreparedStatement result = busConnModel.BeforeUpdate(busConn);
        String expResult = "UPDATE lineStartTime"
                    + " SET  bus= '"+busConn.getBus()+"' , busType = '"+busConn.getBusType()+"' , day= "+busConn.getDay()+" , line= '"+busConn.getLine()
                    +"' , time = '00:20:00+01' "
                    + "WHERE idLineStartTime = ?";
        assertEquals(expResult, result.toString());
    }
    
    /**
     * Test of Insert method, of class BusConnectionModel.
     */
    @Test
    @Tag("UnitTest")
    public void testInsertBusConnection() {
        BusConnectionDTO busConn = new BusConnectionDTO();
        String bus = "TESTbus";
        String busType = "k15s0";
        String line = "TESTline";
        int day = 5;
        Time time = new Time(1,20,0);
        busConn.setBus(bus);
        busConn.setBusType(busType);
        busConn.setDay(day);
        busConn.setLine(line);
        busConn.setTime(time);
        busConnModel.InsertBusConnection(bus, busType, line, day, time.getHours(), time.getMinutes());
        ArrayList<BusConnectionDTO> busscons =  busConnModel.getbusConnIdArray(time.getHours(), time.getMinutes(),busType,bus,line, UpdateSpecification.ALL);     
        if(busscons.size()<=0){
            Assert.fail();
        }
        busConn = busscons.get(busscons.size()-1);
        busConnModel.DeleteBusConnection(busConn.getId());
    }   
}
