/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelTests;

import Enums.DeleteReaction;
import Dto.LineDTO;
import Dto.StopOnRouteDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import Model.LineModel;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class LineModelTest {
    
    LineModel lineModel;
    
    public LineModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        lineModel = new LineModel(); 
    }
    
    @After
    public void tearDown() {
    }   

    /**
     * Test of getStopOnRoute method, of class LineModel.
     */
    @Test
    @Tag("UnitTest")
    public void testGetStopOnRoute() {
        //arrange
        String line = "TESTline";
        String busStop = "TESTbusStop2";
        StopOnRouteDTO expectedStop = new StopOnRouteDTO();
        expectedStop.setBusStop(busStop);
        expectedStop.setBusStopNumber(2);
        expectedStop.setLine(line);
        expectedStop.setTimeFromPrevBusStop(5);
        expectedStop.setId(0);
        //act
        StopOnRouteDTO stop = lineModel.getStopOnRoute(line, busStop);        
        //assert       
        assertEquals(expectedStop.getBusStop(), stop.getBusStop());
        assertEquals(expectedStop.getBusStopNumber(), stop.getBusStopNumber());
        assertEquals(expectedStop.getLine(), stop.getLine());
        assertEquals(expectedStop.getTimeFromPrevBusStop(), stop.getTimeFromPrevBusStop());        
    }

    /**
     * Test of getPrevStopOnRoute method, of class LineModel.
     */
    @Test
    @Tag("UnitTest")
    public void testGetPrevStopOnRoute() {
        //arrange
        String line = "TESTline";
        String busStop = "TESTbusStop2";
        StopOnRouteDTO expectedStop = new StopOnRouteDTO();
        expectedStop.setBusStop("TESTbusStop1");
        expectedStop.setBusStopNumber(1);
        expectedStop.setLine(line);
        expectedStop.setTimeFromPrevBusStop(0);        
        //act
        StopOnRouteDTO stop = lineModel.getPrevStopOnRoute(line, busStop);        
        //assert       
        assertEquals(expectedStop.getBusStop(), stop.getBusStop());
        assertEquals(expectedStop.getBusStopNumber(), stop.getBusStopNumber());
        assertEquals(expectedStop.getLine(), stop.getLine());
        assertEquals(expectedStop.getTimeFromPrevBusStop(), stop.getTimeFromPrevBusStop());       
    }

    /**
     * Test of getNextStopOnRoute method, of class LineModel.
     */
    @Test
    @Tag("UnitTest")
    public void testGetNextStopOnRoute() {
        //arrange
        String line = "TESTline";
        String busStop = "TESTbusStop2";
        StopOnRouteDTO expectedStop = new StopOnRouteDTO();
        expectedStop.setBusStop("TESTbusStop3");
        expectedStop.setBusStopNumber(3);
        expectedStop.setLine(line);
        expectedStop.setTimeFromPrevBusStop(5);        
        //act
        StopOnRouteDTO stop = lineModel.getNextStopOnRoute(line, busStop);        
        //assert       
        assertEquals(expectedStop.getBusStop(), stop.getBusStop());
        assertEquals(expectedStop.getBusStopNumber(), stop.getBusStopNumber());
        assertEquals(expectedStop.getLine(), stop.getLine());
        assertEquals(expectedStop.getTimeFromPrevBusStop(), stop.getTimeFromPrevBusStop());          
    }   
}
