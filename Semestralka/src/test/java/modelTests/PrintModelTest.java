/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelTests;

import Dto.BusConnectionDTO;
import Dto.LineDTO;
import Dto.StopOnRouteDTO;
import java.sql.Time;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import Model.PrintModel;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class PrintModelTest {
    
    PrintModel printModel;
    
    public PrintModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        printModel = new PrintModel();
    }
    
    @After
    public void tearDown() {
    }  

    /**
     * Test of ActualizeTimeToBusStop method, of class PrintModel.
     */
    @Test
    @Tag("UnitTest")
    public void testActualizeTimeToBusStop() {
      //arrange
        BusConnectionDTO busConn = new BusConnectionDTO();
        busConn.setTime(new Time(10,20,0));
        Time addTime = new Time(21,5,0);
        BusConnectionDTO expected = new BusConnectionDTO();
        expected.setTime(new Time(7,25,0));
        expected.setDay(1);
      //act
        BusConnectionDTO result = printModel.ActualizeTimeToBusStop(busConn, addTime);
      //assert
        assertEquals(expected.getTime().getHours(), result.getTime().getHours());
        assertEquals(expected.getTime().getMinutes(), result.getTime().getMinutes());
        assertEquals(expected.getDay(), result.getDay());
    }
   
    /**
     * Test of sumTimeFromStartToBusStop method, of class PrintModel.
     */
    @Test
    @Tag("UnitTest")
    public void testSumTimeFromStartToBusStop() {
        //arrange
        ArrayList<StopOnRouteDTO> stopsOnRoute = new ArrayList<>();
        Time expected = new Time(0,40,0);
        for(int i=0;i<10;i++) {
            StopOnRouteDTO stop = new StopOnRouteDTO();
            String busStop = "busStop"+i;
            stop.setBusStop(busStop);
            stop.setBusStopNumber(i);
            stop.setTimeFromPrevBusStop(10);
            stopsOnRoute.add(stop);
        }
        //act
        Time timeRes = printModel.sumTimeFromStartToBusStop(stopsOnRoute, "busStop3");
        //assert
        assertEquals(expected, timeRes);
    }

    /**
     * Test of sumTimeFromPrevBusStop method, of class PrintModel.
     */
    @Test
    @Tag("UnitTest")
    public void testSumTimeFromPrevBusStop() {
         //arrange
        ArrayList<StopOnRouteDTO> stopsOnRoute = new ArrayList<>();
        Time expected = new Time(0,13,0);
        for(int i=0;i<10;i++) {
            StopOnRouteDTO stop = new StopOnRouteDTO();
            String busStop = "busStop"+i;
            stop.setBusStop(busStop);
            stop.setBusStopNumber(i);
            stop.setTimeFromPrevBusStop(10+i);
            stopsOnRoute.add(stop);
        }
        //act
        Time timeRes = printModel.sumTimeFromPrevBusStop(stopsOnRoute, "busStop3");
        //assert
        assertEquals(expected, timeRes);
    }    
}
