/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelTests;

import Dto.PlaceDTO;
import Model.PlaceModel;
import java.sql.PreparedStatement;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class PlaceModelTest {
    
    
    PlaceModel placeModel;
    
    public PlaceModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        placeModel = new PlaceModel(); 
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of BeforeDelete method, of class PlaceModel.
     */
    @Test
    @Tag("UnitTest")
    public void testBeforeDelete() {
        PlaceDTO place = new PlaceDTO();
        place.setId(1);
        PreparedStatement result = placeModel.BeforeDelete(place);
        String expResult = "DELETE FROM Place"
                    + " WHERE IdPlace"
                    + " NOT IN ( SELECT BusStop.IdPlace FROM CarPark Inner JOIN BusStop ON BusStop.IdPlace = CarPark.IdPlace"
                    + " WHERE BusStop.IdPlace= -1 AND CarPark.IdPlace= -1 ) AND Idplace= -1 ";
        assertEquals(expResult, result.toString());
    }
    
}
