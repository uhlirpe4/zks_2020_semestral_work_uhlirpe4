/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processTests;

import Enums.UpdateSpecification;
import Dto.BusConnectionDTO;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.BusConnectionModel;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class ProcessBusConnectionsTest {
    
    BusConnectionModel busConnectionModel ;
    
    public ProcessBusConnectionsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        busConnectionModel = new BusConnectionModel();
    }
    
    @After
    public void tearDown() {
    }

   
     @Test
     @Tag("ProcessTest")
     public void busConnection_insert_remove() throws InterruptedException {
    //arrange
    String bus = "TESTbus";
    String busType = "k15s0";
    String line = "TESTline";
    int day = 3; 
    int hours  = 15; 
    int minutes = 40;   
    //act
    busConnectionModel.InsertBusConnection(bus, busType, line, day, hours, minutes);     
    ArrayList<BusConnectionDTO> busCons =  busConnectionModel.getbusConnIdArray(hours, minutes, busType, bus, line, UpdateSpecification.ONE);    
    ArrayList<BusConnectionDTO> busConsList =  busConnectionModel.getBusConnectionList(line, 0);
    //assert    
        BusConnectionDTO busConToAssert = null;
        for(BusConnectionDTO busCon : busConsList)
        {
        if(busCon.getBus().equals(bus) && busCon.getTime().getMinutes()==minutes && busCon.getTime().getHours() == hours){
        busConToAssert= busCon;
        }
        }        
         assertNotNull(busConToAssert);
         assertEquals(minutes, busConToAssert.getTime().getMinutes());
         assertEquals(hours, busConToAssert.getTime().getHours());
         assertEquals(day, busConToAssert.getDay()); 
         assertEquals(bus, busConToAssert.getBus());   

    //act
     busConnectionModel.DeleteBusConnection(busCons.get(0).getId());
    //assert
      ArrayList<BusConnectionDTO> busConsDeleted =  busConnectionModel.getbusConnIdArray(hours, minutes, busType, bus, line, UpdateSpecification.ONE);
      assertEquals(0, busConsDeleted.size());
     }

}
