/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processTests;

import Dto.BusDTO;
import Dto.BusTypeDTO;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.BusModel;
import Model.BusTypeModel;
import Model.CarParkModel;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class ProcessBusTest {
    private CarParkModel carParkModel;
    private BusTypeModel busTypeModel;
    private BusModel busModel ;
    public ProcessBusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    carParkModel = new CarParkModel();
    busTypeModel = new BusTypeModel();
    busModel = new BusModel();
    }
    
    @After
    public void tearDown() {
    }

   
     @Test
     @Tag("ProcessTest")
     public void busTest_insertCarpark_insertBustype_insertBus() {
         //arrange
     BusTypeDTO busType = new BusTypeDTO();
     busType.setCapacity(20);
     busType.setLowFloor(true);
     busType.setToilet(true);
     busType.setIdName("k20NTs5");
     busType.setStewardsPlace(5);
     String carPark = "indicationCarpark";
     String carParkStreet = "streetTEST";
     String carParkTown = "townTEST";
     String busIdName = "indicationBus";
     //act
     carParkModel.InsertCarPark( carPark, carParkStreet,carParkTown, 50);
     busTypeModel.Insert(busType);   
     busModel.InsertBus(busIdName, busType.getIdName(), carPark);     
     ArrayList<BusDTO> buses = busModel.getBusList(busType.getIdName(), busIdName);
    //assert
    assertEquals(1, buses.size());
    BusDTO bus = buses.get(0);   
    //act
     carParkModel.DeleteCarPark(carPark, carParkStreet, carParkTown);
     busTypeModel.DeleteBusType(busType.getIdName());
     busModel.Delete(bus);
     ArrayList<BusDTO> busesDelete = busModel.getBusList(busType.getIdName(), busIdName);
     assertEquals(0, busesDelete.size());
     }
}
