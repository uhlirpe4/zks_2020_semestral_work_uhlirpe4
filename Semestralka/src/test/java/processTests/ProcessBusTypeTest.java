/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processTests;

import Dto.BusTypeDTO;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.BusTypeModel;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class ProcessBusTypeTest {
    
    BusTypeModel busType;
    String busTypeName;
    
    public ProcessBusTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        busType = new BusTypeModel();
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    @Tag("ProcessTest")
    public void ProcesTypyAutobusu() {
        //arrange
        busTypeName = busType.generateName(true, false, "45", "0");    // "Nk45s0"        
        BusTypeDTO expectedBusType1 = new BusTypeDTO();
        expectedBusType1.setIdName(busTypeName);
        expectedBusType1.setLowFloor(true);
        expectedBusType1.setToilet(false);
        expectedBusType1.setCapacity(45);
        expectedBusType1.setStewardsPlace(0);
        //act
        busType.Insert(expectedBusType1);
        ArrayList<BusTypeDTO> busTypeList = busType.getBusTypeList(busTypeName);
        //assert
        assertEquals(1, busTypeList.size());
        assertEquals(busTypeName, busTypeList.get(0).getIdName());
        //act
        busType.DeleteBusType(busTypeName);        
        //assert        
        assertTrue(busType.getBusTypeList(busTypeName).isEmpty());

    }
}
