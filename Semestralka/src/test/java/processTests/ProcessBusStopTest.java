/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processTests;

import Dto.BusStopDTO;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.BusStopModel;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class ProcessBusStopTest {
    
    BusStopModel busStop;    
    
    public ProcessBusStopTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        busStop = new BusStopModel();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    @Tag("ProcessTest")
    public void ProcesZastavky() {
        //arrange
        ArrayList<BusStopDTO> busStopArray = new ArrayList();       
            //busStop1
        BusStopDTO expectedBusStop1 = new BusStopDTO();
        expectedBusStop1.setSignification("Dejvicka");
        expectedBusStop1.setStreet("Ulice1");
        expectedBusStop1.setTown("Praha");
        //act
        busStop.InsertBusStop("Dejvicka", "Ulice1", "Praha");
        busStopArray = busStop.getBusStopList(0, "Dejvicka");
        //assert
        assertEquals(1, busStopArray.size());
        assertEquals(expectedBusStop1.getSignification(), busStopArray.get(0).getSignification());
        //act 
        busStop.UpdateBusStop("Dejvicka - metro", "Ulice1", "Praha");
        //arrange   //busStop2
        BusStopDTO expectedBusStop2 = new BusStopDTO();
        expectedBusStop2.setSignification("Dejvicka - metro");
        expectedBusStop2.setStreet("Ulice1");
        expectedBusStop2.setTown("Praha");
        //act 
        busStopArray = busStop.getBusStopList(0,"Dejvicka - metro");
        //assert
        assertEquals(1, busStopArray.size());
        assertEquals(expectedBusStop2.getSignification(), busStopArray.get(0).getSignification());
        //act
        busStop.DeleteBusStop("Dejvicka - metro", "Ulice1", "Praha");
        //assert
        busStopArray = busStop.getBusStopList(0, "Dejvicka - metro");         
        assertTrue(busStopArray.isEmpty());
         
    }
}
