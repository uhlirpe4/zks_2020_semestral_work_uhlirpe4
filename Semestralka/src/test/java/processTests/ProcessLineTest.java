/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processTests;

import Dto.LineDTO;
import Dto.StopOnRouteDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.LineModel;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlíř (uhlirpe4
 */
public class ProcessLineTest {
    
    LineModel line;
    
    public ProcessLineTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        line = new LineModel();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    @Tag("ProcessTest")
    public void ProcesLinky() throws SQLException {
        //arrange
        ArrayList<StopOnRouteDTO> lineArray = new ArrayList();        
        String lineIdName = "Test1";        
            //stopOnRoute1
        StopOnRouteDTO stopOnRoute1 = new StopOnRouteDTO();
        stopOnRoute1.setId(1);
        stopOnRoute1.setBusStop("Kopeček");
        stopOnRoute1.setBusStopNumber(1);
        stopOnRoute1.setLine(lineIdName);
        stopOnRoute1.setTimeFromPrevBusStop(0);
        lineArray.add(stopOnRoute1);
            //stopOnRoute2
        StopOnRouteDTO stopOnRoute2 = new StopOnRouteDTO();
        stopOnRoute2.setId(2);
        stopOnRoute2.setBusStop("černé mosty");
        stopOnRoute2.setBusStopNumber(2);
        stopOnRoute2.setLine(lineIdName);
        stopOnRoute2.setTimeFromPrevBusStop(3);
        lineArray.add(stopOnRoute2);
            //line
        LineDTO line1 = new LineDTO();
        line1.setNumber(lineIdName);  
        //act
        line.InsertLine(lineIdName, lineArray);       
        ArrayList<LineDTO> lineList = line.getLineList(0, lineIdName);
        //assert
        assertEquals(lineIdName, lineList.get(0).getNumber());
        assertEquals(stopOnRoute1.getBusStop(), lineList.get(0).getFirstBusStop());
//        assertEquals(stopOnRoute2.getBusStop(), lineList.get(0).getLastBusStop());
        
        lineArray.remove(1);
        StopOnRouteDTO stopOnRoute3 = new StopOnRouteDTO();
        stopOnRoute3.setBusStop("E55");
        stopOnRoute3.setBusStopNumber(2);
        stopOnRoute3.setLine(lineIdName);
        stopOnRoute3.setTimeFromPrevBusStop(4);
        lineArray.add(1, stopOnRoute3);
        line1.setNumber(lineIdName);
        line1.setFirstBusStop(stopOnRoute1.getBusStop());
        line1.setLastBusStop(stopOnRoute3.getBusStop());
        //act
        line.DeleteStopOnRoute(lineIdName,stopOnRoute2.getBusStop()); 
        ArrayList<StopOnRouteDTO> addedStop = new ArrayList<>();
        addedStop.add(stopOnRoute3);
        line.InsertRoutes(addedStop);
        String newLineIdName = "Test2";
        line.UpdateLine(newLineIdName, lineArray, "Test1");
        lineList = line.getLineList(0, newLineIdName);        
        assertEquals(newLineIdName, lineList.get(0).getNumber());
        assertEquals(stopOnRoute1.getBusStop(), lineList.get(0).getFirstBusStop());
        assertEquals(stopOnRoute3.getBusStop(), lineList.get(0).getLastBusStop());
               
        line.DeleteLine("Test2");        
        lineList.remove(0);        
        assertTrue(lineList.isEmpty());
        
    }
    
}
