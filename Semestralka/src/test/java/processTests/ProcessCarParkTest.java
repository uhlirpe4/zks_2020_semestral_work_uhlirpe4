/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package processTests;

import Dto.CarParkDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.CarParkModel;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class ProcessCarParkTest {

    private CarParkModel carParkModel;

    public ProcessCarParkTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        carParkModel = new CarParkModel();
    }

    @After
    public void tearDown() {
    }

    @Test
    @Tag("ProcessTest")
    public void CarPark_insert_update_remove() {
        //arrange
        String indication = "indicationTESTCarpark";
        String newIndication = "indicationTESTCarpark2";
        String street = "streetTESTCarpark";
        String town = "townTESTCarpark";
        int capacity = 20;
        int newCapacity = 22;
        CarParkDTO data = new CarParkDTO();
        data.setSignification(newIndication);
        //act
        carParkModel.InsertCarPark(indication, street, town, capacity);
        carParkModel.UpdateCarPark(newIndication, street, town, newCapacity);
        CarParkDTO updatedRecord = carParkModel.getById(data);
        //assert
        assertNotNull(updatedRecord);
        assertEquals(newCapacity, updatedRecord.getCapacity());
        assertEquals(newIndication, updatedRecord.getSignification());
        assertEquals(street, updatedRecord.getStreet());
        assertEquals(town, updatedRecord.getTown());
        //act
        carParkModel.DeleteCarPark(newIndication, street, town);        
        CarParkDTO deletedRecord = carParkModel.getById(data);
        //assert
        assertNull(deletedRecord);
    }
}
