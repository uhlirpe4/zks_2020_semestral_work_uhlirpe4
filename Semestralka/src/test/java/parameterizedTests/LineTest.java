/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameterizedTests;

import Dto.StopOnRouteDTO;
import Model.LineModel;
import configClasses.LineSource;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class LineTest {
    
    LineModel linemodel = new LineModel();
    
    @ParameterizedTest
    @ArgumentsSource(LineSource.class)
    @Tag("ParameterizedTest")
    public void testInsertLines(String lineNumber, ArrayList<StopOnRouteDTO> stopsArray){
        linemodel.InsertLine(lineNumber, stopsArray);
        assertEquals(stopsArray.get(0).getBusStop(), linemodel.getStopsOnRouteList(lineNumber).get(0).getBusStop());
        assertEquals(stopsArray.get(stopsArray.size()-1).getBusStop(), linemodel.getLineList(0, lineNumber).get(0).getLastBusStop());
        assertEquals(stopsArray.get(0).getBusStop(), linemodel.getLineList(0, lineNumber).get(0).getFirstBusStop());
        assertEquals(lineNumber, linemodel.getLineList(0, lineNumber).get(0).getNumber());
        assertEquals(stopsArray.get(stopsArray.size()-1).getTimeFromPrevBusStop(), linemodel.getStopOnRoute(lineNumber, linemodel.getLineList(0, lineNumber).get(0).getLastBusStop()).getTimeFromPrevBusStop());
        linemodel.DeleteLine(lineNumber);
    }
    
}
