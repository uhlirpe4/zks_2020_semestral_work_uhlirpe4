/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameterizedTests;

import Model.BusStopModel;
import configClasses.BusStopSource;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Tag;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusStopTest {
    
    BusStopModel busstopmodel = new BusStopModel();
    
    @ParameterizedTest
    @ArgumentsSource(BusStopSource.class)
    @Tag("ParameterizedTest")
    public void testInsertBusStops(String indication, String street, String town){
        busstopmodel.InsertBusStop(indication, street, town);
        assertNotNull(busstopmodel.getBusStop(indication, street, town));
        assertEquals(indication, busstopmodel.getBusStop(indication, street, town).getSignification());
        assertEquals(street, busstopmodel.getBusStop(indication, street, town).getStreet());
        assertEquals(town, busstopmodel.getBusStop(indication, street, town).getTown());
        busstopmodel.DeleteBusStop(indication, street, town);
        assertThrows(NullPointerException.class, () -> {
             busstopmodel.getBusStop(indication, street, town).getSignification();
        }, "exception was not thrown");
    }
    
}
