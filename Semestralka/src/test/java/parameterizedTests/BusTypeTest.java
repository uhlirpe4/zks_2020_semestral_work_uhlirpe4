/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameterizedTests;

import Dto.BusTypeDTO;
import Model.BusTypeModel;
import configClasses.BusTypeSource;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusTypeTest {
    
    BusTypeModel bustypemodel = new BusTypeModel();
    
    @ParameterizedTest
    @ArgumentsSource(BusTypeSource.class)
    @Tag("ParameterizedTest")
    public void testInsertBusTypes(boolean Lowfloor, boolean toilet, String capacity, String StewardsPlace, BusTypeDTO bustypeobject){
        String bustype = bustypemodel.generateName(Lowfloor, toilet, capacity, StewardsPlace);
        bustypemodel.Insert(bustypeobject);
        assertNotNull(bustypemodel.getBusTypeList(bustype).get(0));
        assertEquals(bustype, bustypemodel.getBusTypeList(bustype).get(0).getIdName());
        assertEquals(Lowfloor, bustypemodel.getBusTypeList(bustype).get(0).isLowFloor());
        assertEquals(toilet, bustypemodel.getBusTypeList(bustype).get(0).isToilet());
        assertEquals(capacity, String.valueOf(bustypemodel.getBusTypeList(bustype).get(0).getCapacity()));
        assertEquals(StewardsPlace, String.valueOf(bustypemodel.getBusTypeList(bustype).get(0).getStewardsPlace()));
        bustypemodel.DeleteBusType(bustype);
    }
    
}
