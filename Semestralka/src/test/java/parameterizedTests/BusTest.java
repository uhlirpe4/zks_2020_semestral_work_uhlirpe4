/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parameterizedTests;

import Enums.UpdateSpecification;
import Model.BusModel;
import configClasses.BusSource;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

/**
 *
 * @author Petr Uhlir (uhlirpe4)
 */
public class BusTest {
    
    BusModel busmodel = new BusModel();
    
    @ParameterizedTest
    @ArgumentsSource(BusSource.class)
    @Tag("ParameterizedTest")
    public void testInsertBusConnections(String indication, String busType, String CarPark){
        busmodel.InsertBus(indication, busType, CarPark);
        assertEquals(indication, busmodel.getBusList(busType, indication).get(0).getIndication());
        assertEquals(busType, busmodel.getBusList(busType, indication).get(0).getBusType());
        assertEquals(CarPark, busmodel.getBusList(busType, indication).get(0).getCarPark());
        assertNotNull(busmodel.getBusList(busType, indication).get(0));
        busmodel.DeleteBus(indication);
        assertThrows(IndexOutOfBoundsException.class, () -> {
             busmodel.getBusList(busType, indication).get(0);
        }, "exception was not thrown");
    }
    
}
