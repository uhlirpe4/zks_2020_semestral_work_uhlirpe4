/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configClasses;

import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

/**
 *
 * @author Petr Uhlíř (uhlirpe4)
 */
public class BusStopSource implements ArgumentsProvider {
    
    @Override
    public Stream<? extends Arguments> provideArguments(org.junit.jupiter.api.extension.ExtensionContext ec) throws Exception {
        
        return Stream.of(
                Arguments.of("Pražské předměstí", "Pražské předměstí", "České Budějovice"),
                Arguments.of("Dejvická", "Evropská", "Praha 6"),
                Arguments.of("Náměstí", "Náměstí Přemysla Otakara II.", "České Budějovice"),
                Arguments.of("Karlovo náměstí", "Karlovo náměstí", "Praha"),
                Arguments.of("Václavské náměstí", "Václavské náměstí", "Praha"),
                Arguments.of("Anděl", "Smíchov", "Praha"),
                Arguments.of("Na Knížecí", "Smíchov", "Praha"),
                Arguments.of("Technická knihovna", "Evropská", "Praha 6"),
                Arguments.of("Velké náměstí", "Na náměstí", "České Budějovice")
        );
    }
    
}
