/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configClasses;

import Dto.StopOnRouteDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

/**
 *
 * @author Petr Uhlíř (uhlirpe4)
 */
public class LineSource implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(org.junit.jupiter.api.extension.ExtensionContext ec) throws Exception {
        
        StopOnRouteDTO zastavka1 = new StopOnRouteDTO();
        zastavka1.setId(1);
        zastavka1.setBusStopNumber(1);
        zastavka1.setLine("11");
        zastavka1.setBusStop("Nádraží");
        zastavka1.setTimeFromPrevBusStop(0);
        
        StopOnRouteDTO zastavka2 = new StopOnRouteDTO();
        zastavka2.setId(2);
        zastavka2.setBusStopNumber(2);
        zastavka2.setLine("11");
        zastavka2.setBusStop("U Soudu");
        zastavka2.setTimeFromPrevBusStop(3);
        
        StopOnRouteDTO zastavka3 = new StopOnRouteDTO();
        zastavka3.setId(3);
        zastavka3.setBusStopNumber(1);
        zastavka3.setLine("15");
        zastavka3.setBusStop("U Koníčka");
        zastavka3.setTimeFromPrevBusStop(0);
        
        StopOnRouteDTO zastavka4 = new StopOnRouteDTO();
        zastavka4.setId(4);
        zastavka4.setBusStopNumber(2);
        zastavka4.setLine("15");
        zastavka4.setBusStop("Výstaviště");
        zastavka4.setTimeFromPrevBusStop(15);
        
        StopOnRouteDTO zastavka5 = new StopOnRouteDTO();
        zastavka5.setId(5);
        zastavka5.setBusStopNumber(1);
        zastavka5.setLine("34");
        zastavka5.setBusStop("Nádraží");
        zastavka5.setTimeFromPrevBusStop(0);
        
        StopOnRouteDTO zastavka6 = new StopOnRouteDTO();
        zastavka6.setId(6);
        zastavka6.setBusStopNumber(2);
        zastavka6.setLine("34");
        zastavka6.setBusStop("Poliklinika Sever");
        zastavka6.setTimeFromPrevBusStop(5);
        
        StopOnRouteDTO zastavka7 = new StopOnRouteDTO();
        zastavka7.setId(7);
        zastavka7.setBusStopNumber(3);
        zastavka7.setLine("34");
        zastavka7.setBusStop("U Zelené ratolesti");
        zastavka7.setTimeFromPrevBusStop(7);
        
        return Stream.of(
                Arguments.of("11", new ArrayList<>(Arrays.asList(zastavka1, zastavka2))),
                Arguments.of("15", new ArrayList<>(Arrays.asList(zastavka3, zastavka4))),
                Arguments.of("34", new ArrayList<>(Arrays.asList(zastavka5, zastavka6, zastavka7)))
        );
    }
    
}
