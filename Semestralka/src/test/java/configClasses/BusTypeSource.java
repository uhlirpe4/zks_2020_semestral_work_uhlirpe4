/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configClasses;

import Dto.BusTypeDTO;
import Model.BusTypeModel;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

/**
 *
 * @author Petr Uhlíř (uhlirpe4)
 */
public class BusTypeSource implements ArgumentsProvider {
    
    @Override
    public Stream<? extends Arguments> provideArguments(org.junit.jupiter.api.extension.ExtensionContext ec) throws Exception {
        
        BusTypeModel bustypemodel = new BusTypeModel();
        
        BusTypeDTO busType1 = new BusTypeDTO();
        busType1.setLowFloor(false);
        busType1.setToilet(true);
        busType1.setCapacity(50);
        busType1.setStewardsPlace(1);
        busType1.setIdName(bustypemodel.generateName(busType1.isLowFloor(), busType1.isToilet(), String.valueOf(busType1.getCapacity()), String.valueOf(busType1.getStewardsPlace())));
        
        BusTypeDTO busType2 = new BusTypeDTO();
        busType2.setLowFloor(true);
        busType2.setToilet(false);
        busType2.setCapacity(48);
        busType2.setStewardsPlace(0);
        busType2.setIdName(bustypemodel.generateName(busType2.isLowFloor(), busType2.isToilet(), String.valueOf(busType2.getCapacity()), String.valueOf(busType2.getStewardsPlace())));
        
        return Stream.of(
                Arguments.of(busType1.isLowFloor(), busType1.isToilet(), String.valueOf(busType1.getCapacity()), String.valueOf(busType1.getStewardsPlace()), busType1),
                Arguments.of(busType2.isLowFloor(), busType2.isToilet(), String.valueOf(busType2.getCapacity()), String.valueOf(busType2.getStewardsPlace()), busType2)
        );
    }
    
}
