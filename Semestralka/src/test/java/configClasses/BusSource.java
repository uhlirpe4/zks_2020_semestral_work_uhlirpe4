/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configClasses;

import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import java.util.stream.Stream;

/**
 *
 * @author Petr Uhlíř (uhlirpe4)
 */
public class BusSource implements ArgumentsProvider {
    
    @Override
    public Stream<? extends Arguments> provideArguments(org.junit.jupiter.api.extension.ExtensionContext ec) throws Exception {
        
        return Stream.of(
                Arguments.of("Ikaros2615", "Nk30s0", "TESTcarpark"),
                Arguments.of("Ikaros7615", "Nk52s0", "TESTcarpark"),
                Arguments.of("Ikaros2626", "k15s0", "TESTcarpark")
        );
    }
    
}
